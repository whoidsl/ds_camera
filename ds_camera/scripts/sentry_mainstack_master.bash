#!/bin/bash

#Get the directory this script is located in
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Load the standard ROS stuff
source $DIR/setup.bash

# Set my own IP
export ROS_IP=192.168.100.103

# Look for sentry mainstack running as master
export ROS_MASTER_URI=http://192.168.100.100:11311/
