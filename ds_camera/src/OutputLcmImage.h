#pragma once

#include <string>
#include <jpeglib.h>
#include "AbstractOutput.h"
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include "ds_camera_msgs/core_image_t.h"

/* Output an image over LCM
 * Depending on the framerate, this can REALLY slow stuff down, so there's
 * a downsampling option
 */

#define OUTNAME_LCMIMG "lcmimg"

namespace CamAcq {
class OutputLcmImage : public CamAcq::AbstractOutput {
  public:
    //void init() override;
    OutputLcmImage(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig);
    virtual ~OutputLcmImage();

    virtual void handleImage(const CamAcq::Image::ConstPtr& img);
    virtual void fillInStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {};
    virtual void fillInShutdownStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {};

  protected:
    using CamAcq::AbstractOutput::input;
    std::string imgChannel;
    ros::Publisher image_pub_;

    int downsample;
    int droppedToDownsample;

    bool useJpeg;
    int jpegQuality;
    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;

    uint8_t* imgBuf;
    size_t imgBufSize;

    uint8_t* jpegBuf;
    size_t jpegBufLen;

    void convertImageTo8bit(ds_camera_msgs::core_image_t& pkt,
        const CamAcq::Image::ConstPtr& img);

    void jpegCompress(ds_camera_msgs::core_image_t& pkt,
        const CamAcq::Image::ConstPtr& img);

};
};
