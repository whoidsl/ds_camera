#include "ProcessingThread.h"

#include <string.h>
#include <iostream>
#include <limits>
#include "CameraUtil.h"

using namespace CamAcq;

ProcessThread::ProcessThread(const CameraConfig::Ptr& cfg) {
  // do nothing
}

void ProcessThread::threadMain() {
  if (! input) {
    throw std::logic_error("MUST connect input before running thread!");
  }

  bool valid;
  Image::Ptr img;
  while (running.test_and_set()) {

    // sometimes the input ring queue just returns without a valid image
    // (by design)
    if (! input->pop(img)) {
      continue;
    }

    // if processImage fails, it will return false
    if (!processImage(img)) {
      continue;
    }

    // send to ALL outputs
    for (auto iter=outputs.begin(); iter != outputs.end(); iter++) {
      (*iter)->push(img);
    }
  } // while running

  for (auto iter=outputs.begin(); iter != outputs.end(); iter++) {
    (*iter)->stopWaiting();
  }
}

bool ProcessThread::processImage(const Image::Ptr& img) {
  //std::cout <<"Processing image " <<img.get() <<std::endl;
  try {

    //////////////////////////////////////////////////////////
    // process the image's statistics
    VmbErrorType err = img->raw_frame->GetFrameID(img->frameId);
    checkErrorCode(err);
  
    err = img->raw_frame->GetTimestamp(img->timestamp);
    checkErrorCode(err);
  
    // possibly bail if the image is junk
    err = img->raw_frame->GetReceiveStatus(img->status);
    checkErrorCode(err);
    if (img->status != VmbFrameStatusComplete) {
      std::cerr <<"Frame " <<img.get() <<" was not complete! Aborting!\n";
      //return false;
    }
  
    err = img->raw_frame->GetBufferSize(img->bufferSize);
    checkErrorCode(err);
  
    err = img->raw_frame->GetAncillarySize(img->ancillarySize);
    checkErrorCode(err);
  
    err = img->raw_frame->GetPixelFormat(img->rawFormat);
    checkErrorCode(err);
  
    err = img->raw_frame->GetHeight(img->height);
    checkErrorCode(err);
  
    err = img->raw_frame->GetWidth(img->width);
    checkErrorCode(err);

    err = img->raw_frame->GetOffsetX(img->offsetX);
    checkErrorCode(err);

    err = img->raw_frame->GetOffsetY(img->offsetY);
    checkErrorCode(err);
  
    //////////////////////////////////////////////////////////
    // Handle ancillary data
    loadAncillaryData(img);

    //////////////////////////////////////////////////////////
    // Put into a common format
    unpackImage(img);
  
    //////////////////////////////////////////////////////////
    // Compute some stats
    calculateHistogram(img);
    return true;
  } catch (const std::exception& e) {
    std::cerr <<"EXCEPTION during processing!\n";
    std::cerr <<e.what() <<"\n";
  }
  return false;
}

void ProcessThread::loadAncillaryData(const Image::Ptr& img) {

  VmbErrorType err;

  AVT::VmbAPI::AncillaryDataPtr data;
  AVT::VmbAPI::FeaturePtrVector features;
  err = img->raw_frame->GetAncillaryData(data);
  if (err == VmbErrorNotFound) {
    // there's no ancillary data
    std::cerr <<"No ancillary data on frame! (try turning on chunk mode)\n";
    return;
  }
  checkErrorCode(err);

  err = data->Open();
  checkErrorCode(err);

  err = data->GetFeatures(features);
  checkErrorCode(err);

  for (auto iter = features.begin(); iter != features.end(); iter++) {
    
    CamAcq::Feature::ConstPtr f = CamAcq::FeatureFromDriver(*iter);
    if (f) {
      img->features[f->getName()] = f;
      //std::cout <<"Adding chunk feature " <<f->getName() <<"\n";
    }
  }

  data->Close();

}

void ProcessThread::unpackImage(const Image::Ptr& img) {

  // ********************************************************************
  // setup the source image
  VmbImage srcImage;

  VmbUchar_t* raw_buf;
  VmbErrorType err = img->raw_frame->GetImage(raw_buf);
  checkErrorCode(err);

  if (img->rawFormat == VmbPixelFormatBayerGB12Packed) {
    //std::cout <<"Applying BayerGR12Packed hack" <<std::endl;
    img->rawFormat = VmbPixelFormatBayerRG12Packed;
  }

  srcImage.Data = raw_buf;
  srcImage.Size = sizeof(VmbImage); // driver quirk.  I assume this is a magic number / hacky type safety but... whatever
  err = (VmbErrorType)VmbSetImageInfoFromPixelFormat(img->rawFormat, img->width,
      img->height, &srcImage);
  checkErrorCode(err);

  // ********************************************************************
  // First, we might debayer the image, and definitely need to setup
  // our output.  
  // That will require an intermediate image buffer
  VmbPixelFormatType intermedFormat, destFormat;
  bool convert = true;
  int destBPP=1;
  bool skip_intermed = false;

  switch (img->rawFormat) {
    case VmbPixelFormatMono8:
      prepareOutput(img, VmbPixelFormatMono8, 1);
      copyRawToOutput(srcImage, img);
      break;

    case VmbPixelFormatMono10p:
    case VmbPixelFormatMono12Packed:
    case VmbPixelFormatMono12p:
      prepareOutput(img, VmbPixelFormatMono16, 2);
      unpackPixels(srcImage, img);
      break;

    case VmbPixelFormatMono10:
    case VmbPixelFormatMono12:
    case VmbPixelFormatMono14:
    case VmbPixelFormatMono16: 
      prepareOutput(img, img->rawFormat, 2);
      copyRawToOutput(srcImage, img);
      normalizeDest(img);
      break;

    case VmbPixelFormatBayerGR8:
    case VmbPixelFormatBayerRG8:
    case VmbPixelFormatBayerGB8:
    case VmbPixelFormatBayerBG8:
      prepareOutput(img, VmbPixelFormatRgb8, 3);
      runConvert(srcImage, img);
      break;

    case VmbPixelFormatBayerGR10:
    case VmbPixelFormatBayerRG10:
    case VmbPixelFormatBayerGB10:
    case VmbPixelFormatBayerBG10:
    case VmbPixelFormatBayerGR10p:
    case VmbPixelFormatBayerRG10p:
    case VmbPixelFormatBayerGB10p:
    case VmbPixelFormatBayerBG10p:
      prepareOutput(img, VmbPixelFormatRgb10, 6);
      runConvert(srcImage, img);
      normalizeDest(img); //convert Rgb10 to Rgb16
      break;

    case VmbPixelFormatBayerGR12:
    case VmbPixelFormatBayerRG12:
    case VmbPixelFormatBayerGB12:
    case VmbPixelFormatBayerBG12:
    case VmbPixelFormatBayerGR12Packed:
    case VmbPixelFormatBayerRG12Packed:
    case VmbPixelFormatBayerGB12Packed:
    case VmbPixelFormatBayerBG12Packed:
    case VmbPixelFormatBayerGR12p:
    case VmbPixelFormatBayerRG12p:
    case VmbPixelFormatBayerGB12p:
    case VmbPixelFormatBayerBG12p:
      prepareOutput(img, VmbPixelFormatRgb12, 6);
      runConvert(srcImage, img);
      normalizeDest(img); //convert Rgb12 to Rgb16
      break;

    case VmbPixelFormatBayerGR16:
    case VmbPixelFormatBayerRG16:
    case VmbPixelFormatBayerGB16:
    case VmbPixelFormatBayerBG16:
      prepareOutput(img, VmbPixelFormatRgb16, 6);
      runConvert(srcImage, img);
      break;
    default:
      std::cerr <<"Non-Bayer format! Limiting output to 8-bits!\n";
      prepareOutput(img, VmbPixelFormatRgb8, 3);
      runConvert(srcImage, img);
      break;
  } // switch (img->rawFormat)
}

void ProcessThread::prepareOutput(const Image::Ptr& img, VmbPixelFormatType destFormat, int destBPP) {
  int destSize = img->width * img->height * destBPP;
  img->ensureImageDataSize(destSize);
  img->imageFormat = destFormat;
  img->imageBytes = destSize;
  img->image.Data = img->imageData;
  img->image.Size = sizeof(VmbImage);
  VmbErrorType err = (VmbErrorType)VmbSetImageInfoFromPixelFormat(destFormat, img->width,
      img->height, &img->image);
  checkErrorCode(err);
}

void ProcessThread::unpackPixels(const VmbImage& srcImg, const Image::Ptr& img) {
  // srcImg and img->image should already be assigned.  We just have to execute the copy.

  switch (img->rawFormat) {

    case VmbPixelFormatMono10p:
      std::cerr <<"Documentation on Mono10p seemed a little funky; without anything to test against, we treat like 12p and pray\n#FIXME\n\n";
      // FALLTHROUGH
    case VmbPixelFormatMono12Packed:
    case VmbPixelFormatMono12p:
      // valid; these are all packed into 12 bits, so we just unpack them!
      break;

    default:
      throw std::runtime_error("Can only unpack Mono10p,12Packed and 12p formats! Bayer formats should be demosaiced first, non-packed formats use normalizeDest");
  }// switch format

  uint16_t* dstPtr = reinterpret_cast<uint16_t*>(img->image.Data);
  uint16_t* lastPixel = reinterpret_cast<uint16_t*>(img->image.Data) + img->imageBytes/sizeof(uint16_t);

  for (uint8_t* srcPtr = reinterpret_cast<uint8_t*>(srcImg.Data); dstPtr < lastPixel; srcPtr+=3) {
    *dstPtr++ = (static_cast<uint16_t>(*(srcPtr  )) <<8) + ((static_cast<uint16_t>(*(srcPtr+1)) & 0xf0)     );
    *dstPtr++ = (static_cast<uint16_t>(*(srcPtr+2)) <<8) + ((static_cast<uint16_t>(*(srcPtr+1)) & 0x0f) << 4);
  }

}

void ProcessThread::copyRawToOutput(const VmbImage& srcImg, const Image::Ptr& img) {
  memcpy(img->image.Data, srcImg.Data, img->imageBytes);
}

void ProcessThread::runConvert(const VmbImage& srcImg, const Image::Ptr& img) {
    // Set the transform parameters
    // actually, transform parameter example totally doesn't work, even
    // a little.
    VmbErrorType err = (VmbErrorType)VmbImageTransform(&srcImg, &(img->image), NULL, 0);
    checkErrorCode(err);
}

void ProcessThread::normalizeDest(const Image::Ptr& img) {

  int shift = 0;
  VmbPixelFormatType newFormat = img->imageFormat;

  // only deal with images we've created-- e.g., by debayering
  switch (img->imageFormat) {
    case  VmbPixelFormatMono8:
    case VmbPixelFormatMono16:
    case   VmbPixelFormatRgb8:
    case  VmbPixelFormatRgb16:
      // we're good
      break;

    case VmbPixelFormatMono10:
      shift=6;
      newFormat = VmbPixelFormatMono16;
      break;

    case VmbPixelFormatMono12:
      shift=4;
      newFormat = VmbPixelFormatMono16;
      break;

    case VmbPixelFormatMono14:
      shift=2;
      newFormat = VmbPixelFormatMono16;
      break;

    case  VmbPixelFormatRgb10:
      shift=6;
      newFormat = VmbPixelFormatRgb16;
      break;

    case  VmbPixelFormatRgb12:
      shift=4;
      newFormat = VmbPixelFormatRgb16;
      break;

    case VmbPixelFormatMono10p:
    case VmbPixelFormatMono12Packed:
    case VmbPixelFormatMono12p:
      throw std::runtime_error("Do not call normalize on packed mono formats; call unpack instead!");

    default:
      std::cerr <<"Unknown image format, could not normalize!" <<std::endl;
  } // switch imageFormat

  if (shift == 0 || img->image.ImageInfo.PixelInfo.Alignment == VmbAlignmentMSB) {
    // don't shift, or we're already most-significant-bit aligned, or its an 8-bit image.  Just claim victory!
    //std::cout <<"Stopping early! shift=" <<shift <<", or already MSB-aligned!" <<std::endl;
    return;
  }

  
  // actually dot he shifting
  uint16_t* lastPixel = reinterpret_cast<uint16_t*>(img->image.Data) + img->imageBytes/sizeof(uint16_t);

  for (uint16_t* ptr = reinterpret_cast<uint16_t*>(img->image.Data); ptr < lastPixel; ptr++) {
    (*ptr) <<=shift;
  }

  // re-tag the image with the new type
  VmbErrorType err = (VmbErrorType)VmbSetImageInfoFromPixelFormat(newFormat, img->width,
      img->height, &img->image);
  checkErrorCode(err);
  img->imageFormat = newFormat;

}

void ProcessThread::calculateHistogram(const Image::Ptr& img) {

  const size_t HIST_LEN = 255;
  std::vector<uint32_t> accum(HIST_LEN, 0);
  size_t totalEntries;

  switch (img->imageFormat) {
    case VmbPixelFormatMono16:
      {
        uint16_t* ptr = reinterpret_cast<uint16_t*>(img->imageData);
        totalEntries = (img->imageBytes) / sizeof(uint16_t);
        uint16_t* ptr_done = ptr + totalEntries;
        for (; ptr < ptr_done; ptr++) {
          accum[(*ptr >> 8) & 0x00ff]++; // use only the most significant byte
        }
      }
      break;

    case VmbPixelFormatMono8:
      {
        uint8_t* ptr = reinterpret_cast<uint8_t*>(img->imageData);
        totalEntries = (img->imageBytes) / sizeof(uint8_t);
        uint8_t* ptr_done = ptr + totalEntries;
        for (; ptr < ptr_done; ptr++) {
          accum[*ptr]++; // use only the most significant byte
        }
      }
      break;

    // RGB (color)
    case VmbPixelFormatRgb16:
      {
        uint16_t* ptr = reinterpret_cast<uint16_t*>(img->imageData);
        totalEntries = (img->imageBytes) / (3*sizeof(uint16_t));
        uint16_t* ptr_done = ptr + 3*totalEntries;
        for (; ptr < ptr_done; ptr++) {
          float r = static_cast<float>(*ptr++)/std::numeric_limits<uint16_t>::max();
          float g = static_cast<float>(*ptr++)/std::numeric_limits<uint16_t>::max();
          float b = static_cast<float>(*ptr++)/std::numeric_limits<uint16_t>::max();
          // use the OpenCV style RGB->greyscale 
          // (http://docs.opencv.org/2.4/modules/imgproc/doc/miscellaneous_transformations.html)
          uint8_t bw = static_cast<uint8_t>((0.299 * r + 0.587*g + 0.114*b) * std::numeric_limits<uint8_t>::max());
          accum[bw]++;
        }
      }
      break;

    case VmbPixelFormatRgb8:
      {
        uint8_t* ptr = reinterpret_cast<uint8_t*>(img->imageData);
        totalEntries = (img->imageBytes) / (3*sizeof(uint8_t));
        uint8_t* ptr_done = ptr + 3*totalEntries;
        for (; ptr < ptr_done; ptr++) {
          float r = static_cast<float>(*ptr++)/std::numeric_limits<uint8_t>::max();
          float g = static_cast<float>(*ptr++)/std::numeric_limits<uint8_t>::max();
          float b = static_cast<float>(*ptr++)/std::numeric_limits<uint8_t>::max();
          // use the OpenCV style RGB->greyscale 
          // (http://docs.opencv.org/2.4/modules/imgproc/doc/miscellaneous_transformations.html)
          uint8_t bw = static_cast<uint8_t>((0.299 * r + 0.587*g + 0.114*b) * std::numeric_limits<uint8_t>::max());
          accum[bw]++;
        }
      }
      break;

    // everything else
    default:
      std::cerr<<"Unknown converted image format type!" <<std::endl;
      return;
  }

  double minVal = *std::min_element(accum.begin(), accum.end());
  double maxVal = *std::max_element(accum.begin(), accum.end());
  double scale = static_cast<double>(std::numeric_limits<typename Image::HistEntryT>::max())
    / (maxVal - minVal);
  img->histogram.resize(HIST_LEN);
  for (size_t i=0; i<img->histogram.size(); i++) {
    img->histogram[i] = static_cast<typename Image::HistEntryT>(scale*(static_cast<double>(accum[i])-minVal));
  }
}
