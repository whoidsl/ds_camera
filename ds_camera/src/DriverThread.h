#pragma once

#include <thread>
#include <atomic>


#include <ros/ros.h>
#include "ds_camera_msgs/status_t.h"
#include "ds_camera_msgs/camera_t.h"
#include "ds_camera_msgs/attribute_t.h"
#include "ds_camera_msgs/command_t.h"

namespace CamAcq {

class Thread {
  public:
    typedef std::shared_ptr<Thread> Ptr;
    typedef std::shared_ptr<const Thread> ConstPtr;

    Thread();
    virtual ~Thread() = 0;

    /// \brief Starts the thread
    virtual void start();

    /// \brief Called at the start of main
    virtual void threadMain() = 0;

    /// \brief Stops the thread. Called from outside
    void stop();

    void stop_nonblock();

    /// \brief Fills in status variables
    ///
    /// May be called from a different thread
    ///
    /// state is the driver state structure, to be filled in
    /// statuses is a vector to hold however many 
    ///     status structures are generated
    virtual void fillInStatus(ds_camera_msgs::camera_t &state, std::vector<ds_camera_msgs::status_t> &statuses) = 0;

    /// \brief Fills in status variables right before shutdown
    /// 
    /// May be called from another thread
    /// 
    /// Called right before shutdown so ther driver can 
    /// send reasonable messages at shutdown.
    virtual void fillInShutdownStatus(ds_camera_msgs::camera_t &state, std::vector<ds_camera_msgs::status_t> &statuses) = 0;
    
  protected:
    volatile std::atomic_flag running;

    std::thread inner_thread;
};
};
