#pragma once

#include <boost/noncopyable.hpp>
#include <boost/function.hpp>
#include <mutex>
#include <memory>
#include <vector>

namespace CamAcq {

template <typename T>
class DriverObjectPool : private boost::noncopyable {
  // This is VERY close, but not EXACTLY what boost pool does

  public:
    typedef std::shared_ptr<T> ObjPtr;
    typedef std::shared_ptr<const T> ConstObjPtr;

    typedef std::shared_ptr<DriverObjectPool<T> > Ptr;
    typedef std::shared_ptr<DriverObjectPool<T> > ConstPtr;

    DriverObjectPool();
    virtual ~DriverObjectPool();

    typedef boost::function<T* ()> Allocator_t;
    void preallocate(size_t number, Allocator_t allocator);
    size_t getCapacity() const;
    size_t getFreeCapacity() const;

    // wipe all objects from the queue
    void clear();

    ObjPtr alloc();
    // to free an ObjPtr, just delete as usual!
    
    // need something to step through all the free objects
    

    typedef boost::function<void (const ObjPtr&)> RegisterCallback_t;
    // iterate through and execute the callback on all free objects.
    // The callback is executed for one object at a time
    void mapFreeObjects(RegisterCallback_t callback);


  protected:
    std::mutex myLock;
    typedef std::unique_lock<std::mutex> ulock;

    typedef std::vector<ObjPtr> ObjVector;
    ObjVector objs;

};
};
