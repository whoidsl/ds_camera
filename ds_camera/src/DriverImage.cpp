#include "DriverImage.h"

#include "CameraUtil.h"

using namespace CamAcq;

///////////////////////////////////////////////////////////////////////////////
// Feature stuff
///////////////////////////////////////////////////////////////////////////////

namespace CamAcq {
// these are full specializations, so they have to go here, in the .cpp
// this case won't work so good, so just throw an error

template<>
int32_t Feature::as<int32_t>() const {
  return this->asInt();
}

template<>
int64_t Feature::as<int64_t>() const {
  return this->asInt();
}

template<>
double Feature::as<double>() const {
  return this->asDouble();
}

template<>
float Feature::as<float>() const {
  return this->asDouble();
}
  
template<>
std::string Feature::as<std::string>() const {
  return this->toString();
}

template<>
int FeatureT<std::string>::asInt() const {
  throw std::runtime_error("Feature " + name + " is a string, cannot cast to int!");
}

template<>
double FeatureT<std::string>::asDouble() const {
  throw std::runtime_error("Feature " + name + " is a string, cannot cast to double!");
}
};

Feature::ConstPtr CamAcq::FeatureFromDriver(const AVT::VmbAPI::FeaturePtr& f) {
  VmbFeatureDataType type;
  std::string name;
  Feature::ConstPtr ret;

  VmbErrorType err = f->GetDataType(type);
  if (!checkErrorCode(err, false)) {
    return ret;
  }

  err = f->GetName(name);
  if (!checkErrorCode(err, false)) {
    return ret;
  }

  switch (type) {
    case VmbFeatureDataBool:
      {
        VmbBool_t bValue;
        err = f->GetValue( bValue );
        if (!checkErrorCode(err, false)) {
          return ret;
        }
        ret.reset(new FeatureBool(name, bValue));
      }
      break;

    case VmbFeatureDataEnum:
      {
        std::string eValue;
        err = f->GetValue( eValue );
        if (!checkErrorCode(err, false)) {
          return ret;
        }
        ret.reset(new FeatureEnum(name, eValue));
      }
      break;
    
    case VmbFeatureDataFloat:
      {
        double fValue;
        err = f->GetValue( fValue );
        if (!checkErrorCode(err, false)) {
          return ret;
        }
        ret.reset(new FeatureFloat(name, fValue));
      }
      break;
    
    case VmbFeatureDataInt:
      {
        VmbInt64_t iValue;
        err = f->GetValue( iValue );
        if (!checkErrorCode(err, false)) {
          return ret;
        }
        ret.reset(new FeatureInt(name, iValue));
      }
      break;

    case VmbFeatureDataString:
      {
        std::string sValue;
        err = f->GetValue( sValue );
        if (!checkErrorCode(err, false)) {
          return ret;
        }
        ret.reset(new FeatureString(name, sValue));
      }
      break;
          
    case VmbFeatureDataCommand:
    default:
      {
        // do nothing
      }
      break;
  } // switch(type)

  return ret; // MAY return NULL!

}

///////////////////////////////////////////////////////////////////////////////
// Image class
///////////////////////////////////////////////////////////////////////////////
Image::Image(size_t payloadSize) {
  // initialize the raw frame
  SP_SET(raw_frame, new AVT::VmbAPI::Frame(payloadSize));
  
  forceNewFile = false;
  resetStats();

  imageData = NULL;
  imageDataSize = 0;

  intermedData = NULL;
  intermedDataSize = 0;
}

Image::~Image() {
  if (imageData) {
    free(imageData);
    imageData = NULL;
    imageDataSize = 0;
  }
  if (intermedData) {
    free(intermedData);
    intermedData = NULL;
    intermedDataSize = 0;
  }
}

void Image::resetStats() {
  status = VmbFrameStatusInvalid;
  rawFormat = (VmbPixelFormatType)0;
  imageFormat = (VmbPixelFormatType)0;
  imageBytes = 0;
  ancillarySize = 0;
  bufferSize = 0;
  height=0;
  width=0;
  frameId=0;
  timestamp=0;
  utime=0;

  offsetX=0;
  offsetY=0;
}

void Image::ensureImageDataSize(size_t bytes) {
  if (imageData && bytes <= imageDataSize) {
    return;
  }
  if (imageData) {
    free(imageData);
  }
  imageData = calloc(1, bytes);
  imageDataSize = bytes;
}

void Image::ensureIntermedDataSize(size_t bytes) {
  if (intermedData && bytes <= intermedDataSize) {
    return;
  }
  if (intermedData) {
    free(intermedData);
  }
  intermedData = calloc(1, bytes);
  intermedDataSize = bytes;
}

