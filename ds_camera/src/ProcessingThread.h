#pragma once

// A block that takes images from the camera
// and processes them (possibly de-bayer, probably histograms, etc)

#include "CameraConfig.h"
#include "DriverThread.h"
#include "DriverQueue.h"
#include "DriverImage.h"
#include <vector>

namespace CamAcq {
class ProcessThread : public CamAcq::Thread {
  public:
    typedef std::shared_ptr<ProcessThread> Ptr;
    typedef std::shared_ptr<const ProcessThread> ConstPtr;
  
    ProcessThread(const CameraConfig::Ptr& cfg);
    virtual ~ProcessThread() = default;
  
    typedef CamAcq::DriverQueue<CamAcq::Image::ConstPtr> OutputQueue_t;
    void addOutput(const typename OutputQueue_t::Ptr& out) { outputs.push_back(out); };
  
    typedef CamAcq::DriverQueue<CamAcq::Image::Ptr> InputQueue_t;
    void setInput(const typename InputQueue_t::Ptr& in) { input = in; };

    virtual void threadMain();
    virtual void fillInStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {}; // do nothing

    virtual void fillInShutdownStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {}; // do nothing


  protected:
    InputQueue_t::Ptr input;
    std::vector<OutputQueue_t::Ptr> outputs;

    void loadAncillaryData(const Image::Ptr& img);
    bool processImage(const Image::Ptr& img);
    void unpackImage(const Image::Ptr& img);

    void prepareOutput(const Image::Ptr& img, VmbPixelFormatType destFormat, int destBPP);
    void unpackPixels(const VmbImage& srcImg, const Image::Ptr& img);
    void copyRawToOutput(const VmbImage& srcImg, const Image::Ptr& img);
    void runConvert(const VmbImage& srcImg, const Image::Ptr& img);
    void normalizeDest(const Image::Ptr& img);
    void calculateHistogram(const Image::Ptr& img);

};
};
