#pragma once

#include <string>

extern "C" {
#include <SDL.h>
#include <SDL_ttf.h>
}

#include "AbstractOutput.h"

#define OUTNAME_SDL "sdl"

namespace CamAcq {
class OutputSdl : public CamAcq::AbstractOutput {
  public:
    //void init() override;
    OutputSdl(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig);
    virtual ~OutputSdl();

    virtual void start() { /* do nothing, MUST be run by main thread */ };
    virtual void threadMain(); // we need to do our own thing, because SDL events

    virtual void handleImage(const CamAcq::Image::ConstPtr& img);
    virtual void fillInStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {};
    virtual void fillInShutdownStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {};

    virtual bool isDisplayOutput() const { return true; };


  protected:
    std::string camName;
    SDL_Surface* screen;
    TTF_Font *font;
    SDL_Color font_color;
    CamAcq::Image::ConstPtr lastImage;

    int uvDownsample; // only display every Nth row and column
    int histHeightPct;
    int histV0; // Start of the histogram

    // inner functions
    TTF_Font* loadFont(const std::string& name, int size) const; // NOT re-entrant
    void initScreen(const CamAcq::Image::ConstPtr& img);
    void handleEvent(SDL_Event& event);
    void handleKeyboard(SDL_Event& event);
    void handleResize(SDL_Event& event);

    // drawing functions
    void drawImage(const CamAcq::Image::ConstPtr& img);
    void drawImagePixels(const CamAcq::Image::ConstPtr& img);
    void drawFrameNumber(const CamAcq::Image::ConstPtr& img);
    void drawTimestamp(const CamAcq::Image::ConstPtr& img);
    void drawHistogram(const CamAcq::Image::ConstPtr& img);

};
};

