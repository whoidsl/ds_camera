#include "OutputSdl.h"

#include <fontconfig/fontconfig.h>
#include <iostream>
#include <algorithm>
#include "CameraUtil.h"

#include <SDL_gfxPrimitives.h>

#include <boost/date_time/posix_time/posix_time.hpp>

using namespace CamAcq;

#define DEFAULT_FONT_NAME "Roboto"
#define DEFAULT_FONT_SIZE 14
#define DEFAULT_FONT_R 255
#define DEFAULT_FONT_G   0
#define DEFAULT_FONT_B   0

OutputSdl::OutputSdl(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig) : AbstractOutput(driverConfig, outputConfig) {

  // setup SDL
  if ( SDL_Init (SDL_INIT_VIDEO ) == -1 ) {
		throw std::runtime_error("Unable to initialize SDL!");
	}
  TTF_Init();
  screen = NULL;

  // On screen text is red
    font_color.r = DEFAULT_FONT_R;
    font_color.g = DEFAULT_FONT_G;
    font_color.b = DEFAULT_FONT_B;

  // load a font
  std::string fontname;
  int fontsize;

  for (auto elem : outputConfig) {
      if (elem.first == "fontName") {
          fontname = static_cast<std::string>(elem.second);
      }
      else {
          fontname = DEFAULT_FONT_NAME;
      }
  }

  for (auto elem : outputConfig) {
      if (elem.first == "fontSize") {
          fontsize = static_cast<int>(elem.second);
      }
      else {
          fontsize = DEFAULT_FONT_SIZE;
      }
  }

  font = loadFont(fontname, fontsize);

  camName = driverConfig->getName();

  int width= 680;
  int height = 512;
  uvDownsample = 2;
  int posX = -1;
  int posY = -1;
  for (auto elem : outputConfig) {
      if (elem.first == "uvDownsample") {
          uvDownsample = static_cast<int>(elem.second);
      } else if (elem.first == "initialWidth") {
          width = static_cast<int>(elem.second);
      } else if (elem.first == "initialHeight") {
          height = static_cast<int>(elem.second);
      } else if (elem.first == "positionX") {
          posX = static_cast<int>(elem.second);
      } else if (elem.first == "positionY") {
          posY = static_cast<int>(elem.second);
      } else {
          ROS_INFO_STREAM("some sdl window elements not found in output params");
      }
  }

      // load the maptastic icon (obviously!)
      SDL_Surface *icon = SDL_LoadBMP(getIconPath().c_str());
      SDL_WM_SetIcon(icon, NULL);
      SDL_FreeSurface(icon);

      //std::cout <<"Intializing to " <<width <<"x" <<height <<std::endl;
      // create an initial window-- its less confusing that way
      screen = SDL_SetVideoMode(width, height, 0, SDL_HWSURFACE);
      if (screen == NULL) {
          throw std::runtime_error("Unable to initialize SDL screen!");
      }
      std::string windowCaption = "\"" + camName + "\" Driver";
      SDL_WM_SetCaption(windowCaption.c_str(), NULL);

      // set an initial window position
      //
      // This doesn't seem to work in Ubuntu 16.04.  It looks like
      // no SDL_VIDEO_WINDOW_POS variable gets defined by the
      // underlying whatever.  It might, maybe, be possible to get access
      // to the underlying X window through SDL and manipulate it directly
      // through libx or something, but that seems like a lot of work
      // for a minor feature change.  So, you know, if you get a chance to
      // address that, that'd be cool
      //std::cout <<"\n\nGot position: " <<posX <<"," <<posY <<"\n\n";
      /*
      if (posX >= 0 && posY >= 0) {
        std::stringstream windowPosition;
        windowPosition <<"SDL_VIDEO_WINDOW_POS=" <<posX <<"," <<posY;
        std::cout <<"Initial position: \"" <<SDL_getenv("SDL_VIDEO_WINDOW_POS") <<"\"\n";
        std::cout <<"setting position with:\n" <<windowPosition.str() <<"\n\n";
        SDL_putenv(const_cast<char*>(windowPosition.str().c_str())); // SDL const bug?
      }
      // */
      for (auto elem : outputConfig) {
          if (elem.first == "histogramHeight") {
              histHeightPct = static_cast<int>(elem.second);
          } else {
              histHeightPct = 20;
          }
      }
  }

OutputSdl::~OutputSdl() {
  if (font != NULL) {
		TTF_CloseFont(font);
		font = NULL;
	}

	TTF_Quit();
	SDL_Quit();
}

void OutputSdl::threadMain() {
  if (!input) {
    throw std::logic_error("MUST connect input before running thread");
  }

  // make sure we stop
  SDL_Event event;
  bool valid;
  Image::ConstPtr img;
  while (running.test_and_set()) {
    while (SDL_PollEvent(&event)) {
      handleEvent(event);
    }

    // sometimes pop returns without a valid image (timeout, etc)
    if (! input->pop_timeout(img, 100) ) {
      continue;
    }

    // handle image
    handleImage(img);
  }
  running.clear();
}

void OutputSdl::handleImage(const CamAcq::Image::ConstPtr& img) {
  lastImage = img;
  drawImage(lastImage);
}

TTF_Font* OutputSdl::loadFont(const std::string& name, int size) const {

  // Ok, first we have to find the desired font, using fontconfig
	// This is strangely complicated, so we've annotated all
	// the function calls
	FcPattern *pattern, *fpat;
	FcResult result = FcResultMatch;
	FcChar8 *ttfFilename;

	// initalize the fontconfig library`
	if (!FcInit()) {
		throw std::invalid_argument("SDLOUT::loadFonts()::FcInit FAILED!");
	}

	// parse the desired font name into a pattern
	if ((pattern = FcNameParse((const FcChar8*)name.c_str())) == NULL) {
		throw std::invalid_argument("SDLOUT::loadFonts()::FcNameParse FAILED!");
	}
	
	if ((fpat = FcFontMatch(NULL, pattern, &result)) == NULL || result != FcResultMatch) {
		throw std::invalid_argument("SDLOUT::loadFonts()::FcFontMatch FAILED!");
	}
	
	// Get the font filename for the fft file
	if (FcPatternGetString(fpat, FC_FILE, 0, &ttfFilename) != FcResultMatch) {
		throw std::runtime_error("SDLOUT::loadFonts()::FcPatternGetString FAILED!");
	}

  TTF_Font* ret = TTF_OpenFont((const char*)ttfFilename, size);
	if (ret == NULL) {
		throw std::runtime_error(
        std::string("SDLOUT::loadFonts()::TTF_OpenFont FAILED!") 
        + TTF_GetError());
	}

  FcPatternDestroy(fpat);
	FcPatternDestroy(pattern);
	FcFini();
	ttfFilename = NULL;

  return ret;
}

void OutputSdl::initScreen(const CamAcq::Image::ConstPtr& img) {
  int width, height;

  auto iterW = img->features.find("WidthMax");
  if (iterW == img->features.end()) {
    std::cout <<"Could not get max width, using image width... (update camera firmware?)\n";
    width = img->width;
  } else {
    width = (iterW->second)->asInt();
  }
  width /= uvDownsample;

  auto iterH = img->features.find("HeightMax");
  if (iterH == img->features.end()) {
    std::cout <<"Could not get max height, using image height... (update camera firmware?)\n";
    height = img->height;
  } else {
    height = (iterH->second)->asInt();
  }
  height /= uvDownsample;
  histV0 = height;

  int histPct = std::min(90, std::max(0, histHeightPct));
  height = static_cast<int>(height / (1.0f - static_cast<float>(histPct)/100.0f));

  if (screen == NULL || screen->w != width || screen->h != height) {
    // create or resize:
    //std::cout <<"Setting screen to: " <<width <<"x" <<height <<std::endl;
    screen = SDL_SetVideoMode(width, height, 0, SDL_HWSURFACE);
    if (screen == NULL) {
      throw std::runtime_error("Unable to initialize SDL screen!");
    }
  }
}

void OutputSdl::handleEvent(SDL_Event& event) {
  switch (event.type) {
    //case SDL_VIDEORESIZE:
    //  std::cout <<"Got resize event...\n";
    //  handleResize(event);
    //  break;
    case SDL_KEYDOWN:
      handleKeyboard(event);
      break;


    case SDL_ACTIVEEVENT:
      drawImage(lastImage);
      break;

    case SDL_QUIT:
      std::cout <<"Force Quitting SDL\n";
      break;

  } // switch event type
}

void OutputSdl::handleKeyboard(SDL_Event& event) {
  //std::cout <<"KEY: " <<event.key.keysym.sym <<"\n";
  switch(event.key.keysym.sym) {
    case SDLK_h:
      std::cout <<"Keyboard Command Help for cam-driver: \n";
      std::cout <<" [ -- make window smaller\n";
      std::cout <<" ] -- make window bigger\n";
      break;

    case SDLK_LEFTBRACKET:
      if (uvDownsample < 10) {
        uvDownsample++;
      } else {
        uvDownsample = 10;
      }

      break;

    case SDLK_RIGHTBRACKET:
      if (uvDownsample > 1) {
        uvDownsample--;
      } else {
        uvDownsample = 1;
      }
      break;
  } 
}

void OutputSdl::handleResize(SDL_Event& event) {
  if (event.resize.w < screen->w*90/100 
      && event.resize.h < screen->h*90/100) {
    // it's a make things smaller event!
    uvDownsample++; // so downsample MORE
    
  } else if (event.resize.w > screen->w*110/100 
      && event.resize.h > screen->h*110/100) {
    // it's a make things bigger event!
    uvDownsample--; // so downsample LESS
  }
}

void OutputSdl::drawImage(const CamAcq::Image::ConstPtr& img) {
  // wipe the screen
  if (SDL_MUSTLOCK(screen)) {
  	SDL_LockSurface(screen);
	}
  memset(screen->pixels, 0, (screen->h)*(screen->pitch));
  if (SDL_MUSTLOCK(screen)) {
  	SDL_UnlockSurface(screen);
	}

  if (img.get() == NULL) {
    SDL_UpdateRect(screen, 0, 0, screen->w, screen->h);
    // aaaand abort
    return;
  }

  // initialize
  initScreen(img);

  // draw the basic image
  drawImagePixels(img);

  // draw the image histogram
  drawHistogram(img);

  // add annotations
  drawFrameNumber(img);
  drawTimestamp(img);
  
  // update the UI
  SDL_UpdateRect(screen, 0, 0, screen->w, screen->h);
}

void OutputSdl::drawImagePixels(const CamAcq::Image::ConstPtr& img) {
  
  int raw_pixel_stride;
  int raw_offset_r;
  int raw_offset_g;
  int raw_offset_b;

  switch (img->imageFormat) {
    
    // Mono (B&W)
    case VmbPixelFormatMono8:
      raw_pixel_stride = 1;
      raw_offset_r = 0;
      raw_offset_g = 0;
      raw_offset_b = 0;
      break;
      
    case VmbPixelFormatMono16:
      raw_pixel_stride = 2;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
      raw_offset_r = 1;
      raw_offset_g = 1;
      raw_offset_b = 1;
#else
      raw_offset_r = 0;
      raw_offset_g = 0;
      raw_offset_b = 0;
#endif
      break;

    // RGB (color)
    case VmbPixelFormatRgb8:
      raw_pixel_stride = 3;
      raw_offset_r = 0;
      raw_offset_g = 1;
      raw_offset_b = 2;
      break;

    case VmbPixelFormatRgb16:
      raw_pixel_stride = 6;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
      raw_offset_r = 1;
      raw_offset_g = 3;
      raw_offset_b = 5;
#else
      raw_offset_r = 0;
      raw_offset_g = 2;
      raw_offset_b = 4;
#endif
      break;

    // everything else
    default:
      std::cerr<<"Unknown converted image format type!" <<std::endl;
      return;
  } // switch pixel format

  uint8_t R,G,B;
  uint32_t destPixel;
  int32_t offsetX = img->offsetX / uvDownsample;
  int32_t offsetY = img->offsetY / uvDownsample;

  if (SDL_MUSTLOCK(screen)) {
    SDL_LockSurface(screen);
  }


  uint8_t* srcPtr = reinterpret_cast<uint8_t*>(img->image.Data);
  uint8_t* destPtr = reinterpret_cast<uint8_t*>(screen->pixels);

  for (size_t v=0; v<img->height/uvDownsample; v++) {
      
    srcPtr = reinterpret_cast<uint8_t*>(img->image.Data)
        + (v*uvDownsample) * img->width * raw_pixel_stride;
    destPtr = reinterpret_cast<uint8_t*>(screen->pixels)
        + (v+offsetY) * (screen->pitch) + screen->format->BytesPerPixel * (offsetX);

    for (size_t u=0; u<img->width/uvDownsample; u++) {
      R = *(srcPtr + raw_offset_r);
      G = *(srcPtr + raw_offset_g);
      B = *(srcPtr + raw_offset_b);

      destPixel = SDL_MapRGB(screen->format, R, G, B);

      switch(screen->format->BytesPerPixel) {
        case 1: {
            *destPtr = static_cast<uint8_t>(destPixel);
            break;
        }
        case 2: {
            *reinterpret_cast<uint16_t*>(destPtr) = static_cast<uint16_t>(destPixel);
            break;
        }
        case 3: {
            *(destPtr+ screen->format->Rshift/8) = R;
            *(destPtr+ screen->format->Gshift/8) = G;
            *(destPtr+ screen->format->Bshift/8) = B;
            break;
        }
        case 4: {
            *reinterpret_cast<uint32_t*>(destPtr) = static_cast<uint32_t>(destPixel);
            break;
        }
      } // switch screen bytes-per-pixel

      // advance the pointers
      srcPtr += (raw_pixel_stride*uvDownsample);
      destPtr += screen->format->BytesPerPixel;

    }// for u
  } // for v

  if (SDL_MUSTLOCK(screen)) {
    SDL_UnlockSurface(screen);
  }

}


void OutputSdl::drawFrameNumber(const CamAcq::Image::ConstPtr& img) {
  SDL_Surface *toRender;
  SDL_Rect dest;
  std::stringstream textToWrite;
  textToWrite <<"IMG#" <<std::setw(9) <<img->frameId;
  toRender = TTF_RenderText_Solid(font, textToWrite.str().c_str(), font_color);

  // blit the frame number to the screen
  if (SDL_MUSTLOCK(screen)) {
    SDL_LockSurface(screen);
  }

  dest.x = std::max(0,screen->w - toRender->w);
  dest.y = std::max(0,histV0 - toRender->h);
  dest.w = std::min(toRender->w, screen->w);
  dest.h = toRender->h;
  SDL_BlitSurface(toRender, NULL, screen, &dest);

  if (SDL_MUSTLOCK(screen)) {
    SDL_UnlockSurface(screen);
  }
  SDL_FreeSurface(toRender);

}

void OutputSdl::drawTimestamp(const CamAcq::Image::ConstPtr& img) {
  SDL_Surface *toRender;
  SDL_Rect dest;

  time_t tt = img->utime / 1000000;
  int usec = img->utime % 1000000;
  boost::posix_time::ptime tv = boost::posix_time::from_time_t(tt);

  std::stringstream datetimestr;
  datetimestr <<boost::posix_time::to_simple_string(tv);
  datetimestr <<".";
  datetimestr <<std::setw(6) <<std::setfill('0') <<usec; 
  datetimestr <<" UTC";

  toRender = TTF_RenderText_Solid(font, datetimestr.str().c_str(), font_color);

  // blit the timestamp
  if (SDL_MUSTLOCK(screen)) {
    SDL_LockSurface(screen);
  }

  dest.x = 0;
  dest.y = std::max(0,histV0 - toRender->h);
  dest.w = std::min(toRender->w, screen->w);
  dest.h = toRender->h;

  SDL_BlitSurface(toRender, NULL, screen, &dest);

  if (SDL_MUSTLOCK(screen)) {
    SDL_UnlockSurface(screen);
  }
  SDL_FreeSurface(toRender);

}

void OutputSdl::drawHistogram(const CamAcq::Image::ConstPtr& img) {

  typename CamAcq::Image::HistEntryT minval, maxval;
  minval = *std::min_element(img->histogram.begin(), img->histogram.end());
  maxval = *std::max_element(img->histogram.begin(), img->histogram.end());

  if (SDL_MUSTLOCK(screen)) {
    SDL_LockSurface(screen);
  }

  // figure out our parameters
  float dx = static_cast<float>(screen->w) / static_cast<float>(img->histogram.size());
  float dy = static_cast<float>(screen->h - histV0) / static_cast<float>(maxval - minval);

  // clear the existing histogram
  memset(reinterpret_cast<uint8_t*>(screen->pixels) + histV0*screen->pitch,
      0, (screen->h - histV0)*screen->pitch);

  // draw the new histogram 
  int16_t ymax = 0;
  for (size_t i=0; i<img->histogram.size(); i++) {
    //lineRGBA(screen, static_cast<int16_t>(dy*i), screen->h,
      //static_cast<int16_t>(dy*i), static_cast<int16_t>(screen->h - dx*(1+img->histogram[i]-minval)),
      //255, 255, 255, 255);
    boxRGBA(screen, static_cast<int16_t>(dx*i+dy-1), screen->h,
      static_cast<int16_t>(dx*i), static_cast<int16_t>(screen->h - dy*(1+img->histogram[i]-minval)),
      255, 255, 255, 255);
    ymax = std::max(ymax, static_cast<int16_t>(screen->h - dy*(1+img->histogram[i]-minval)));
  }
  
  if (SDL_MUSTLOCK(screen)) {
    SDL_UnlockSurface(screen);
  }

}
