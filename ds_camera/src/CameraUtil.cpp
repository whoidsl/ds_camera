#include "CameraUtil.h"

#include <iostream>
#include <stdexcept>
#include <sys/time.h>

#include <boost/filesystem.hpp>

std::string errorCodeToString(const VmbErrorType& err) {
  switch( err )  {
    case VmbErrorSuccess:           return "Success";
    case VmbErrorInternalFault:     return "Unexpected fault in VmbApi or driver";
    case VmbErrorApiNotStarted:     return "API not started";
    case VmbErrorNotFound:          return "Not found";
    case VmbErrorBadHandle:         return "Invalid handle";
    case VmbErrorDeviceNotOpen:     return "Device not open";
    case VmbErrorInvalidAccess:     return "Invalid access";
    case VmbErrorBadParameter:      return "Bad parameter";
    case VmbErrorStructSize:        return "Wrong DLL version";
    case VmbErrorMoreData:          return "More data returned than memory provided";
    case VmbErrorWrongType:         return "Wrong type";
    case VmbErrorInvalidValue:      return "Invalid value";
    case VmbErrorTimeout:           return "Timeout";
    case VmbErrorOther:             return "TL error";
    case VmbErrorResources:         return "Resource not available";
    case VmbErrorInvalidCall:       return "Invalid call";
    case VmbErrorNoTL:              return "TL not loaded";
    case VmbErrorNotImplemented:    return "Not implemented";
    case VmbErrorNotSupported:      return "Not supported";
    default:                        return "Unknown";
  }
}

bool checkErrorCode(const VmbErrorType& err, bool throwException) {
  if (err == VmbErrorSuccess) {
    return true;
  }
  if (throwException) {
    throw std::runtime_error(errorCodeToString(err));
  } else {
    std::cerr <<errorCodeToString(err) <<std::endl;
  }
  return false;
}

int64_t getUtimeNow() {
  struct timeval tv;
  int rc = gettimeofday(&tv, NULL);
  if (rc) {
    return -1;
  }

  return static_cast<int64_t>(tv.tv_sec)*1000000 + tv.tv_usec;
};

std::string getIconPath() {
  boost::filesystem::path source_file(__FILE__);
  boost::filesystem::path share = source_file.parent_path().parent_path().parent_path();
  share /= "share";
  share /= "maptastic_32.bmp";

  return share.string();
}
