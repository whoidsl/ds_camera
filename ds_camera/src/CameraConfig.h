#pragma once

#include <boost/shared_ptr.hpp>

#include <string>
#include <cstdint>

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <ds_camera/AvtVimbaCameraConfig.h>
#include "ds_camera_msgs/command_t.h"

#include <yaml-cpp/yaml.h>
#include <boost/iterator/iterator_adaptor.hpp>

class CameraConfig {
  public:
    typedef boost::shared_ptr<CameraConfig> Ptr;
    typedef boost::shared_ptr<const CameraConfig> ConstPtr;

    //CameraConfig(const std::string& filename, const std::string& camName);
    CameraConfig();
    //CameraConfig(const YAML::Node& configRoot, const std::string& camName);

    const std::string& getName() const { return uid; };
    const std::string& getUid() const { return uid; };
    const std::string& getPublishPath() const { return publishPath; };
    const std::string& getSystemName() const { return systemName; };
    const std::string& getTriggerId() const { return triggerID; };
    std::string getSurveyID();
    size_t getNumFrames();
    bool isLogToDisk() { return logToDisk; };


    float getUpdateInterval();
    double getCameraStatusImportance();
    double getDriverStatusImportance();

    void setupParameters();
    void dynamicParamCallback(ds_camera::AvtVimbaCameraConfig &config, uint32_t level);
    void updateAttrConfig(std::string param_name, std::string param_value);


    XmlRpc::XmlRpcValue getOutputs() const { return outputParams; };

    size_t numAttributes();
    const std::string nodeName = ros::this_node::getName();

    // ROS Params
    std::string systemName;
    std::string name;
    std::string uid;
    bool vimbaApi;
    std::string triggerID;
    std::string surveyID, frameID;
    double cameraStatusImportance;
    double driverStatusImportance;
    int numFrames;
    std::string trigTimestampTopic;
    std::string publishPath;
    float updateInterval;
    bool logToDisk, publishToRos;
    XmlRpc::XmlRpcValue outputParams;

  protected:
    ros::NodeHandle nh;
    ds_camera_msgs::command_t cmd;
    ros::Publisher cmd_msg_pub_;


    //Dynamically Reconfigurable Params

    //Acquisition
    int acquisitionCmd;
    std::string acquisitionMode;
    double acquisitionFrameRateAbs;

    //Trigger
    std::string triggerSource, triggerMode, triggerSelector, triggerDelay;

    //Exposure
    double exposure;
    std::string exposureAuto, exposureAutoAlg;
    int exposureAutoTol, exposureAutoMax, exposureAutoMin, exposureAutoOutliers, exposureAutoRate, exposureAutoTarget;

    //Gain
    double gain;
    std::string gainAuto;
    int gainAutoTol, gainAutoOutliers, gainAutoRate, gainAutoTarget;
    double gainAutoMax, gainAutoMin;

    //White Balance
    double balanceRatioAbs;
    std::string balanceRatioSelector, whiteBalanceAuto;
    int whiteBalanceAutoTol, whiteBalanceAutoRate;

    //Binning & Decimation
    int binningHorizontal, binningY, decimationX, decimationY;

    //ROI
    int width, height, roiWidth, roiHeight, roiOffsetX, roiOffsetY;

    //Pixel Format
    std::string pixelFormat;

    //Bandwidth
    std::string bandwidthControlMode;
    int streamBytesPerSecond;

    //PTP
    std::string ptpMode;

    //STROBE
    int strobeDelay, strobeDuration;
    std::string strobeDurationMode, strobeSource;

    //GPIO
    std::string syncInSelector, syncOutPolarity, syncOutSelector, syncOutSource;

    //IRIS
    int irisAutoTarget, irisVideoLevelMin, irisVideoLevelMax;
    std::string irisMode;

    void loadConfig();

private:
    boost::shared_ptr<dynamic_reconfigure::Server<ds_camera::AvtVimbaCameraConfig>> dr_srv_;
    dynamic_reconfigure::Server<ds_camera::AvtVimbaCameraConfig>::CallbackType cb_;

};
