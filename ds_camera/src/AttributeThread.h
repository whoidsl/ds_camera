#pragma once

/* A thread to manage interactions, input attributes, etc
 */

#include "DriverThread.h"
#include "CameraThread.h"
#include "CameraConfig.h"
#include <ros/ros.h>
#include <string>

#include "ds_camera_msgs/attrCmd.h"
#include "ds_camera_msgs/powerCmd.h"
#include "ds_camera_msgs/surveyCmd.h"
#include "ds_camera_msgs/camera_t.h"
#include "ds_camera_msgs/status_t.h"
#include "ds_camera_msgs/command_t.h"

namespace CamAcq {
class AttributeThread : public CamAcq::Thread {
  public:
    typedef std::shared_ptr<AttributeThread> Ptr;
    typedef std::shared_ptr<const AttributeThread> ConstPtr;

    AttributeThread(const CameraConfig::Ptr& cfg);
    virtual ~AttributeThread();

    virtual void threadMain();
    virtual void fillInStatus(ds_camera_msgs::camera_t& state,
            std::vector<ds_camera_msgs::status_t>& statuses) {};
    virtual void fillInShutdownStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {}; // do nothing

    void addThread(const CamAcq::Thread::Ptr& _t) { threads.push_back(_t); };
    void setAcqThread(const CamAcq::CamThread::Ptr& _t) { acqThread = _t; };

    bool powerCmd(ds_camera_msgs::powerCmd::Request& req, ds_camera_msgs::powerCmd::Response& res);
    bool surveyCmd(ds_camera_msgs::surveyCmd::Request& req, ds_camera_msgs::surveyCmd::Response& res);
    bool attrCmd(ds_camera_msgs::attrCmd::Request& req, ds_camera_msgs::attrCmd::Response& res);

    void handleCmd(const ds_camera_msgs::command_t& msg);
    void sendStatus();
    void sendShutdownStatus();

  protected:

    ros::NodeHandle nh;
    ros::ServiceServer power_srv_;
    ros::ServiceServer survey_srv_;
    ros::ServiceServer attr_srv_;

    ros::Publisher state_msg_pub_;
    ros::Publisher channel_status_pub_;
    ros::Publisher channel_cmd_pub_;

    ros::Subscriber channel_cmd_sub_;

    std::string channelStatus;
    std::string channelState;
    std::string channelCmd;
    std::string surveyID;



    float updateInterval;

    std::vector<CamAcq::Thread::Ptr> threads;
    CamAcq::CamThread::Ptr acqThread;

};
};
