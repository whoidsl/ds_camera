#pragma once

#include <map>
#include <string>
#include <boost/function.hpp>
#include <boost/functional/factory.hpp>
#include <yaml-cpp/yaml.h>

#include "CameraConfig.h"
#include "AbstractOutput.h"

/* Factory class for outputs
 */

namespace CamAcq {
class OutputFactory {
  public:
    OutputFactory();

    typedef boost::function<CamAcq::AbstractOutput*(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig)> Factory;

    void registerClass(const std::string& type, const Factory& newCons);
    CamAcq::AbstractOutput::Ptr makeObj(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue outputConfig);
  protected:
    std::map<std::string, Factory> factories;
};
};
