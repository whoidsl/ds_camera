#include "OutputConsole.h"

#include <stdexcept>
#include <iostream>
#include <iomanip>

using namespace CamAcq;

OutputConsole::OutputConsole(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig)
  : AbstractOutput(driverConfig, outputConfig) {
  // do nothing
}

void OutputConsole::handleImage(const CamAcq::Image::ConstPtr& img) {
  std::cout <<"OUTPUT got image " <<img.get() <<std::endl;
}
