#include "AVTCamera.h"

#include <sstream>
#include <iostream>
#include <iomanip>
#include <thread>
#include <cstring>
#include <algorithm>

#include <boost/bind.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/construct.hpp>

#include "DriverObject.h"
#include "CameraUtil.h"


class CamObserver : public AVT::VmbAPI::ICameraListObserver {
  public:
    CamObserver(AVTCamera& _cam) : camera(_cam) { };
    virtual ~CamObserver() = default;

    virtual void CameraListChanged(AVT::VmbAPI::CameraPtr pCam, AVT::VmbAPI::UpdateTriggerType reason) {
      //std::cout <<"Got camera event: ";
      if (!camera.isCorrectCam(pCam)) {
          std::cout<<"incorrect cam\n";
        return;
      }

      switch (reason) {
        case AVT::VmbAPI::UpdateTriggerType::UpdateTriggerPluggedIn:
          camera.plugged = true;
          //std::cout <<"PLUGGED!\n";
          break;
        case AVT::VmbAPI::UpdateTriggerType::UpdateTriggerPluggedOut:
          camera.plugged = false;
          //std::cout <<"UNPLUGGED!\n";
          camera.disconnect();
          break;
        case AVT::VmbAPI::UpdateTriggerType::UpdateTriggerOpenStateChanged:
          // do nothing-- just included for completeness
          //std::cout <<"CHANGED!\n";
          break;
      }
    }

  protected:
    AVTCamera& camera;
};

class FrameObserver : public AVT::VmbAPI::IFrameObserver {
  public:
    FrameObserver(AVTCamera& _cam) : IFrameObserver(_cam.handle), camera(_cam) { };
    virtual ~FrameObserver() = default;


    virtual void FrameReceived(const AVT::VmbAPI::FramePtr frame) {
      camera.handleFrame(frame);
    }

  protected:
    AVTCamera& camera;
};

class EventObserver : public AVT::VmbAPI::IFeatureObserver {
  public:
    //EventObserver(AVTCamera& _cam) : IFeatureObserver(_cam.handle), camera(_cam) { };
    EventObserver(AVTCamera& _cam) : camera(_cam) { };
    virtual ~EventObserver() = default;

    virtual void FeatureChanged( const AVT::VmbAPI::FeaturePtr &feature ) {
      if (feature == NULL) {
        std::cerr <<"Got NULL feature (?)" <<std::endl;
        return;
      }

      std::string featureName;
      VmbErrorType err;

      err = feature->GetName(featureName);
      if (!checkErrorCode(err, false)) {
        return;
      }

      AVT::VmbAPI::FeaturePtr f = camera.featureTable.at(featureName);
      //std::cout <<"Updated feature " <<featureName <<" to " <<camera.featureToString(f) <<"\n";
    }

  protected:
    AVTCamera& camera;
};

class FrameFiredObserver : public AVT::VmbAPI::IFeatureObserver {
  public:
    FrameFiredObserver(AVTCamera& _cam) : camera(_cam) { };
    virtual ~FrameFiredObserver() = default;

    virtual void FeatureChanged( const AVT::VmbAPI::FeaturePtr &feature ) {
      int64_t utime = getUtimeNow();
      if (feature == NULL) {
        std::cerr <<"Got NULL feature (?)" <<std::endl;
        return;
      }

      std::string featureName;
      VmbErrorType err;

      err = feature->GetName(featureName);
      if (!checkErrorCode(err, false)) {
        return;
      }

      VmbInt64_t value;
      err = feature->GetValue(value);
      if (!checkErrorCode(err, false)) {
        return;
      }

      if (endsWith(featureName, "Timestamp")) {
        camera.frameTriggerTime(value, utime);
      }

    }

  bool endsWith(const std::string& full, const std::string& ending) const {
    if (full.length() >= ending.length()) {
      return (0 == full.compare (full.length() - ending.length(), ending.length(), ending));
    } else {
      return false;
    }
  }

  protected:
    AVTCamera& camera;
};


AVTCamera::AVTCamera(const CameraConfig::Ptr& cfg): config(cfg), it_(nh), frame_observer(new FrameObserver(*this)) {
  ready = false;
  plugged = false;
  capturing = false;
  continueOldFile.clear();
  lastFrameClockOffset = false;
  cameraTimestampFreqHz = 1;
  
  // avoid re-allocating frames until we call init for the first time
  payloadSize = std::numeric_limits<VmbInt64_t>::max(); 

  numFrames = config->getNumFrames();
  //std::cout<<"numFrames is: "<<numFrames<<"\n";

  //img_pub_ = it_.advertise("image", 1);
}

void AVTCamera::initAPI() {
  AVT::VmbAPI::VimbaSystem& sys = AVT::VmbAPI::VimbaSystem::GetInstance();
  VmbErrorType err = sys.Startup();
  checkErrorCode(err);

  err = sys.RegisterCameraListObserver(AVT::VmbAPI::ICameraListObserverPtr( new CamObserver(*this)));
  checkErrorCode(err);

  startCaptureCalled = false;
}

AVTCamera::~AVTCamera() {
  // disconnect
  disconnect();

  // Close the Vimba API
  AVT::VmbAPI::VimbaSystem& sys = AVT::VmbAPI::VimbaSystem::GetInstance();
  sys.Shutdown();
}


bool AVTCamera::isCorrectCam(const AVT::VmbAPI::CameraPtr& _handle) {
  std::string id;
  VmbErrorType err = _handle->GetID(id);
  checkErrorCode(err);
  //ROS_ERROR_STREAM("id: "<<config->getUid());
  return id == config->getUid();
}

void AVTCamera::connect() {
  AVT::VmbAPI::VimbaSystem& sys = AVT::VmbAPI::VimbaSystem::GetInstance();
  AVT::VmbAPI::CameraPtr _handle;
  //ROS_ERROR_STREAM("uid is now: "<<config->getUid().c_str());
  VmbErrorType err = sys.GetCameraByID(config->getUid().c_str(), _handle);
  if (err != VmbErrorNotFound && !SP_ISNULL(_handle)) {
    checkErrorCode(err);
    plugged = true;
    // we're about to connect
  } else {
    std::cout <<"Camera \"" <<config->getUid() <<"\" not found, waiting for it to plug...\n";
    return;
  }
  try {
      handle = _handle;

      // Print some info about the camera
      std::cout << "------------------------------------------\n";
      std::cout << "Camera \"" << config->getUid() << "\" plugged...\n";
      printCameraDetails(handle);
      std::cout << "\n" << std::endl;

      // Open the camera
      VmbAccessModeType accessMode;
      //err = sys.OpenCameraByID(uid.c_str(), VmbAccessModeFull, _handle);
      std::string camID;
      err = handle->GetID(camID);
      err = handle->GetPermittedAccess(accessMode);
      checkErrorCode(err);
      if (VmbErrorSuccess == err) {
          //ROS_WARN_STREAM("Opening camera");
          err = handle->Open(VmbAccessModeFull);
          if (VmbErrorSuccess == err) {
              ROS_WARN_STREAM("Camera Successfully Opened");
          } else {
              ROS_ERROR_STREAM("Could not open cam: " << err);
          }
      } else {
          ROS_ERROR_STREAM("exception: " << err<<" on cam opening");
      }

    //err = handle->Open(VmbAccessModeFull);
    checkErrorCode(err);
    // Get a handle on the attributes
    err = handle->GetFeatures( features );
    checkErrorCode(err);
    loadFeatures();
    //printFeatures();
    nh.getParam(nodeName+"/"+"vimba_api_check", isVimba);
    if (isVimba) {
        initFeatureObservers();
    }
    else {
        ROS_WARN_STREAM("Camera too old for event control features!");
    }
    //initFeatureObservers();

    // Setup frame stuff
    initCapture(numFrames);
  
    // OK, ready to start acquisition!
    ready = true;
    ROS_WARN_STREAM("ready to set capture params!");
  } catch (const std::exception& err) {
    std::cerr <<"Unable to initialize camera \"" <<config->getUid() <<"\"\n";
    std::cerr <<err.what() <<std::endl;
    ready = false;
  }
}

void AVTCamera::loadFeatures() {
  VmbErrorType err;
  for (AVT::VmbAPI::FeaturePtrVector::const_iterator iter = features.begin();
      iter != features.end(); iter++) {
    std::string name, category;
    err = (*iter)->GetName(name);
    checkErrorCode(err);
  
    featureTable[name] = (*iter);
  }
}

void AVTCamera::printFeatures() {
  VmbErrorType err;

  AVT::VmbAPI::FeaturePtrVector features2;
  err = handle->GetFeatures( features2 );
  checkErrorCode(err);
  std::cout <<"Got new features vector (" <<features2.size() <<")\n";
  std::cout <<"Features: (" <<features.size() <<")" <<"\n";
  for (AVT::VmbAPI::FeaturePtrVector::const_iterator iter = features.begin();
      iter != features.end(); iter++) {
    std::string name, category;
    err = (*iter)->GetName(name);
    checkErrorCode(err);
    err = (*iter)->GetCategory(category);
    checkErrorCode(err);

    std::string rep;
    rep = featureToString(*iter);
    std::cout <<"\t" <<std::setw(40) <<category <<"/" <<std::setw(30) <<name <<": " <<rep <<"\n";
  }
}
void AVTCamera::initCapture(size_t n_frames) {

  std::cout <<"Initializing capture for " <<n_frames <<" frames!\n";

  // Get the frame size
  payloadSize = 0;
  VmbErrorType err;
  if (isVimba) {
      err = featureTable["ChunkModeActive"]->SetValue(true);
      checkErrorCode(err);
  }
  else {
      ROS_WARN_STREAM("Camera too old for chunk mode features!");
  }
      err = featureTable["PayloadSize"]->GetValue(payloadSize);
      checkErrorCode(err);
  // clear the existing pool-- best way to deal with dangling stuff
  err = handle->RevokeAllFrames();
  checkErrorCode(err);

  std::unique_lock<std::mutex> lock(img_queue_mutex); // auto-unlocks when destroyed
  img_pool.clear();
  img_queue.clear();
  lock.unlock();

  // Initialize all the frames
  // first, allocate
  img_pool.preallocate(8, boost::lambda::bind(boost::lambda::new_ptr<CamAcq::Image>(), payloadSize));

  // now, announce
  img_pool.mapFreeObjects(boost::bind(&AVTCamera::announceImage_nolock, this, _1));


  // we're now ready for startCapture
}

void AVTCamera::initFeatureObservers() {

  VmbErrorType err;
  AVT::VmbAPI::IFeatureObserverPtr evtObs(new FrameFiredObserver(*this));
  if (featureTable.find("EventExposureStartTimestamp") != featureTable.end()) {
    err = featureTable["EventExposureStartTimestamp"]->RegisterObserver(evtObs);
    checkErrorCode(err);

    err = featureTable["EventSelector"]->SetValue("ExposureStart");
    checkErrorCode(err);

    std::cout <<"Setting time based on ExposureStart\n";

  } else if (featureTable.find("EventExposureEndTimestamp") != featureTable.end()) {

    err = featureTable["EventExposureEndTimestamp"]->RegisterObserver(evtObs);
    checkErrorCode(err);

    err = featureTable["EventSelector"]->SetValue("ExposureEnd");
    checkErrorCode(err);

    std::cout <<"Setting time based on ExposureEnd\n";
    
  } else {

    err = featureTable["EventFrameTriggerTimestamp"]->RegisterObserver(evtObs);
    checkErrorCode(err);

    err = featureTable["EventSelector"]->SetValue("FrameTrigger");
    checkErrorCode(err);

    std::cout <<"Setting time based on FrameTrigger\n";
  }

  // setup features
  err = featureTable["EventNotification"]->SetValue("On");
  checkErrorCode(err);

  VmbInt64_t rawFreq;
  err = featureTable["GevTimestampTickFrequency"]->GetValue(rawFreq);
  checkErrorCode(err);
  cameraTimestampFreqHz = static_cast<int64_t>(rawFreq);
  std::cout <<"Using timestamp frequency of " <<cameraTimestampFreqHz <<"\n";
  continueOldFile.clear();

  //featureTable["EventFrameTriggerTimestamp"]->RegisterObserver(evtObs);
  //featureTable["EventFrameTriggerFrameID"]->RegisterObserver(evtObs);

};

void AVTCamera::garbageCollectImages_nolock() {
  img_pool.mapFreeObjects(boost::bind(&AVTCamera::requeueImage_nolock, this, _1));
}

void AVTCamera::announceImage_nolock(const CamAcq::Image::Ptr& img) {
  //std::cout <<"ANNOUNCE! " <<std::endl;
  img->raw_frame->RegisterObserver(frame_observer);
  handle->AnnounceFrame(img->raw_frame);
}

void AVTCamera::requeueImage_nolock(const CamAcq::Image::Ptr& img) {
  //std::cout <<"RE-QUEUE! " <<std::endl;
  img_queue[SP_ACCESS(img->raw_frame)]= img;
  handle->QueueFrame(img->raw_frame);
}



void AVTCamera::printCameraDetails(const AVT::VmbAPI::CameraPtr& cam) {

  VmbErrorType err;
  std::string name, modelName, serialNumber, interfaceId, ID;

  err = cam->GetName(name);
  checkErrorCode(err);
  err = cam->GetModel(modelName);
  checkErrorCode(err);
  err = cam->GetSerialNumber(serialNumber);
  checkErrorCode(err);
  err = cam->GetInterfaceID(interfaceId);
  checkErrorCode(err);
  err = cam->GetID(ID);
  checkErrorCode(err);

  std::cout <<"\t       ID: " <<ID <<"\n";
  std::cout <<"\t     Name: " <<name <<"\n";
  std::cout <<"\t    Model: " <<modelName <<"\n";
  std::cout <<"\t   Serial:" <<serialNumber <<"\n";
  std::cout <<"\tInterface: " <<interfaceId <<"\n";
}

void AVTCamera::startCapture() {
  handle->StartCapture();

  std::unique_lock<std::mutex> lock(img_queue_mutex); // auto-unlocks when destroyed
  img_pool.mapFreeObjects(boost::bind(&AVTCamera::requeueImage_nolock, this, _1));
}

void AVTCamera::startAcquisition() {
  continueOldFile.clear();
  VmbErrorType err = featureTable["AcquisitionStart"]->RunCommand();
  checkErrorCode(err);
  capturing = true;
}

void AVTCamera::stopAcquisition() {
  VmbErrorType err = featureTable["AcquisitionStop"]->RunCommand();
  checkErrorCode(err);
  capturing = false;
}

void AVTCamera::stopCapture() {
  VmbErrorType err;
  err = handle->EndCapture();
  checkErrorCode(err);

  // return all frames to the user-- we'll Queue them again on the next startCapture
  err = handle->FlushQueue();
  checkErrorCode(err);

  std::unique_lock<std::mutex> lock(img_queue_mutex); // auto-unlocks when destroyed
  img_queue.clear();
}

void AVTCamera::disconnect() {
  ready = false;

  if (!SP_ISNULL(handle)) {
    VmbErrorType err = handle->Close();
    checkErrorCode(err);

    std::cout <<"Camera \"" <<config->getUid() <<"\" unplugged..." <<std::endl;
    std::cout <<"********************************\n";
    std::cout <<"Disconnecting!\n";
    std::cout <<"********************************\n" <<std::endl;
  }
  SP_RESET(handle);
  features.resize(0);
  featureTable.clear();
}

std::string AVTCamera::getFeature(const std::string& key) const {
  AVT::VmbAPI::FeaturePtr f = featureTable.at(key);
  return featureToString(f);
}

std::string AVTCamera::getFeature(size_t idx, std::string& key) const {
  AVT::VmbAPI::FeaturePtr f = features[idx];
  VmbErrorType err = f->GetName(key);
  checkErrorCode(err);
  return featureToString(f);
}

std::string AVTCamera::featureToString(const AVT::VmbAPI::FeaturePtr& f) const {
  VmbFeatureDataType type;

  VmbErrorType err = f->GetDataType(type);
  checkErrorCode(err);

  std::stringstream ret;

  switch (type) {
    case VmbFeatureDataBool:
      {
        VmbBool_t bValue;
        err = f->GetValue( bValue );
        checkErrorCode(err);
        ret <<bValue;
      }
      break;

    case VmbFeatureDataEnum:
      {
        std::string eValue;
        err = f->GetValue( eValue );
        checkErrorCode(err);
        ret <<eValue;
      }
      break;
    
    case VmbFeatureDataFloat:
      {
        double fValue;
        err = f->GetValue( fValue );
        checkErrorCode(err);
        ret <<fValue;
      }
      break;
    
    case VmbFeatureDataInt:
      {
        VmbInt64_t iValue;
        err = f->GetValue( iValue );
        checkErrorCode(err);
        ret <<iValue;
      }
      break;

    case VmbFeatureDataString:
      {
        std::string sValue;
        err = f->GetValue( sValue );
        checkErrorCode(err);
        ret <<sValue;
      }
      break;
          
    case VmbFeatureDataCommand:
    default:
      {
        ret <<"[None]";
      }
      break;
  } // switch(type)

  return ret.str();
}

void AVTCamera::setFeature(const std::string& key, const std::string& value) {
  AVT::VmbAPI::FeaturePtr f = featureTable.at(key);
  setFeature_inner(f, value);
}

void AVTCamera::setFeature(size_t idx, const std::string& value) {
  AVT::VmbAPI::FeaturePtr f = features[idx];
  setFeature_inner(f, value);
}

void AVTCamera::setFeature_inner(const AVT::VmbAPI::FeaturePtr& f, const std::string& value) {
  std::string name;
  VmbErrorType err = f->GetName(name);
  checkErrorCode(err);
  std::cout <<"Driver wants to set feature \"" <<name <<"\" to \"" <<value <<"\"\n";

  VmbFeatureDataType type;
  err = f->GetDataType(type);
  checkErrorCode(err);

  bool writeable;
  err = f->IsWritable(writeable);
  checkErrorCode(err);

  if (!writeable) {
    std::cout <<"Feature \"" <<name <<"\" not writeable, aborting...\n";
  }

  switch (type) {
    case VmbFeatureDataBool:
      {

        VmbBool_t bValue;
        std::string upperValue(value);
        std::transform(upperValue.begin(), upperValue.end(),
            upperValue.begin(), ::toupper);
        if (upperValue == "ON" || upperValue == "1" || upperValue == "TRUE") {
          bValue = true;
        } else if (upperValue == "OFF" || upperValue == "0" || upperValue == "FALSE") {
          bValue = false;
        } else {
          std::cerr <<"Value \"" <<value <<"\" not parseable as a boolean\n";
          return;
        }
        err = f->SetValue( bValue );
        checkErrorCode(err);
      }
      break;

    case VmbFeatureDataEnum:
      {
        bool isAvail;
        err = f->IsValueAvailable(value.c_str(), isAvail);

        if (isAvail) {
          err = f->SetValue( value.c_str() );
          checkErrorCode(err);
        }
      }
      break;
    
    case VmbFeatureDataFloat:
      {
        double fValue = std::stof(value, nullptr);
        double minval, maxval;
        err = f->GetRange(minval, maxval);
        checkErrorCode(err);

        // clamp the inputs
        fValue = std::max(minval, fValue);
        fValue = std::min(maxval, fValue);
        err = f->SetValue( fValue );
        checkErrorCode(err);
      }
      break;
    
    case VmbFeatureDataInt:
      {
        VmbInt64_t iValue = std::stoi(value, nullptr);

        VmbInt64_t minval, maxval;
        err = f->GetRange(minval, maxval);
        checkErrorCode(err);

        // clamp the inputs
        iValue = std::max(minval, iValue);
        iValue = std::min(maxval, iValue);

        err = f->SetValue( iValue );
        checkErrorCode(err);
      }
      break;

    case VmbFeatureDataString:
      {
        err = f->SetValue( value.c_str() );
        checkErrorCode(err);
      }
      break;
          
    case VmbFeatureDataCommand:
      {
        err = f->RunCommand();
        checkErrorCode(err);
      }
      break;
    default:
      {
        std::cerr <<"Unknown command type!" <<std::endl;
      }
      break;
  } // switch(type)

  // check to see if the payload size changed
  VmbInt64_t newPayload;
  err = featureTable["PayloadSize"]->GetValue(newPayload);
  checkErrorCode(err);

  // if the options made images bigger...
  // we're gonna need a bigger buffer
  if (newPayload > payloadSize) {
    std::cout <<"CAM: New feature requires larger payload size, re-allocating\n";
    stopAcquisition();
    stopCapture();

    // re-initialize capture
    initCapture(numFrames);
  }
}

void AVTCamera::setOnFrameReady(FrameReady_t callback) {
  frameReadyCB = callback;
}

void AVTCamera::handleFrame(const AVT::VmbAPI::FramePtr& frame) {
  // this gets called by a thread managed by the API, so it's important
  // to synchronize access to the img_pool and img_queue

  std::unique_lock<std::mutex> lock(img_queue_mutex); // auto-unlocks when destroyed

  // first, find the frame in the img_queue
  //std::map <AVT::VmbAPI::Frame*, CamAcq::Image::Ptr>::iterator iter
  auto iter = img_queue.find(SP_ACCESS(frame));

  CamAcq::Image::Ptr newFrame;
  if (iter != img_queue.end()) {
    newFrame = iter->second;
    img_queue.erase(iter); // remove from queue so that the imgPool works correctly
  } else {
    std::cerr <<"Got unrecognized frame (WTF?) address!" <<std::endl;
  }

  // run garbage collection
  garbageCollectImages_nolock();

  // ok, we don't need the lock anymore
  lock.unlock();

  // pass to the driver
  if (newFrame && frameReadyCB) {
    //std::cout <<"GOT valid frame "<< newFrame.get() <<"! Sending to driver..." <<"\n";

    newFrame->resetStats();

    // recover the correct timestamp
    int64_t offset = getClockOffset();
    VmbErrorType err;
    VmbUint64_t tstamp;
    err = newFrame->raw_frame->GetTimestamp(tstamp);
    if (!checkErrorCode(err, false)) {
        tstamp = 0;
    }
    if (isVimba) {
        int64_t tstamp_us = static_cast<int64_t>(
                (static_cast<double>(tstamp) * 1.0e6) / cameraTimestampFreqHz);
        //ROS_ERROR_STREAM("tstamp_us: " << tstamp_us);
        newFrame->utime = tstamp_us + offset;
    }
    else {
        ROS_WARN_STREAM_ONCE("Using timestamping for older cameras!");
        int64_t tstamp_us = static_cast<int64_t>(
                (static_cast<double>(tstamp) * 0.01) / cameraTimestampFreqHz);
        //ROS_ERROR_STREAM("tstamp_us: " << tstamp_us);
        newFrame->utime = tstamp_us + offset;
    }
    // copy all features
    newFrame->features.clear();
    for (AVT::VmbAPI::FeaturePtrVector::const_iterator iter = features.begin();
                iter != features.end(); iter++) {
      CamAcq::Feature::ConstPtr f = CamAcq::FeatureFromDriver(*iter);
      if (f) {
        newFrame->features[f->getName()] = f;
      }
    }
    newFrame->forceNewFile = !continueOldFile.test_and_set();
    //std::cout <<"CLOCK OFFSET: " <<std::fixed <<std::setprecision(3);
    //std::cout <<static_cast<double>(offset)/1000.0 <<" ms\n";
    frameReadyCB(newFrame);
  }

}

void AVTCamera::frameTriggerTime(VmbInt64_t cameraTime, int64_t computerTime) {
  int64_t cameraTimeMicrosec = static_cast<int64_t>(
      (static_cast<double>(cameraTime) * 1.0e6)/cameraTimestampFreqHz);

  std::unique_lock<std::mutex> lock(cameraTimeMutex);
  lastFrameClockOffset = computerTime - cameraTimeMicrosec;
  // mutex automagically unlocks on destruction
}

int64_t AVTCamera::getClockOffset() {

  // we want:
  // computerTime = cameraTime + offset
  //
  // Then, to get the time of an image, run:
  // computerTimestamp = cameraTimestamp + getClockOffset()

  std::unique_lock<std::mutex> lock(cameraTimeMutex);
  return lastFrameClockOffset;

}

