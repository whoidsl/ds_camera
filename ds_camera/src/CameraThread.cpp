#include "CameraThread.h"

#include "CameraUtil.h"

#include <unistd.h>
#include <iostream>
#include "ds_camera_msgs/status_t.h"
#include "ds_camera_msgs/camera_t.h"

#include <boost/bind.hpp>

#define STATUS_OK 0
#define STATUS_ALERT 1
#define STATUS_WARN 2
#define STATUS_ERROR 3
#define STATE_RUNNING 0
#define STATE_READY 1
#define STATE_WAITING_FOR_CAMERA 2
#define STATE_WAITING_FOR_SURVEY 3
#define STATE_QUIT 4

CamAcq::CamThread::CamThread(const CameraConfig::Ptr& cfg) :
  config(cfg), camera(cfg), output(new typename CamAcq::CamThread::OutputQueue_t(64, "ImgAcqOut")) {
  // have the camera push new frames directly onto our output queue
  // this is equivalent to 
  // camera.setOnFrameReady(output.push) in python

  //camera.setOnFrameReady(boost::bind(&CamAcq::DriverQueue<CamAcq::Image::Ptr>::push, output.get(), _1));
  camera.setOnFrameReady(boost::bind(&CamAcq::CamThread::frameCallback, this, _1));

  statusTtlSeconds = cfg->updateInterval * 5.0f;
  statusImportance = cfg->driverStatusImportance
    + cfg->cameraStatusImportance;
  //ROS_ERROR_STREAM("statusTTLseconds: " <<statusTtlSeconds);
}


void CamAcq::CamThread::threadMain() {
  
  std::cout <<"CAM: Looking for camera..." <<std::endl;
  camera.initAPI();
  //ROS_ERROR_STREAM("Past initAPI");

  while (running.test_and_set()) {
    // wait for the camera to plug (if it isn't yet!)
    while (!camera.isPlugged() && running.test_and_set()) {
      usleep(50000);
    }
    if (!camera.isPlugged()) {
      running.clear();
      std::cout <<"CAM: STOPPING before camera plug!" <<std::endl;
      output->stopWaiting();
      return;
    }
    if (!camera.isConnected()) {
      camera.connect();
    }

    // start imaging!
    while (camera.isConnected() && running.test_and_set()) {
      // TODO
      usleep(50000);
    }
    if (camera.isConnected()) {
      std::cout <<"CAM: STOPPING with camera plugged!" <<std::endl;
      camera.stopAcquisition();
      camera.stopCapture();
      break;
    } else {
      std::cout <<"CAM: Camera unplugged!" <<std::endl;
    }

  } // while running.test_and_set()
  running.clear();

  // Tell the next thread waiting on our output
  // that nothing is coming
  camera.disconnect();
  output->stopWaiting();
}


void CamAcq::CamThread::fillInStatus(ds_camera_msgs::camera_t &state,
        std::vector<ds_camera_msgs::status_t> &statuses) {

  // start the driver's overall status message
  ds_camera_msgs::status_t status;

  status.utime = getUtimeNow();
  status.key = config->name;
  status.label = config->name;
  status.alertOnWarning = false;
  status.alertOnError = true;
  status.errorOnExpire = true;
  status.ttl_seconds = statusTtlSeconds;
  status.importance = statusImportance;
  status.message = config->name;

  // start state message
  state.utime = status.utime;
  state.camID = config->name;
  state.triggerID = config->triggerID;
  state.surveyID = config->surveyID;

  // figure out what state we're in, and fill in fields accordingly
  if (!camera.isConnected()) {
    state.state = STATE_WAITING_FOR_CAMERA;
    status.display = "NO CAM";
    status.status = STATUS_ERROR;
    status.message += " is not detected (power?)";
  } else {
    // fill in features, get imaging state
    state.attributes.resize(camera.numFeatures());
    try {
      for (size_t i=0; i<state.attributes.size(); i++) {
        std::string key;
        state.attributes[i].object = "cam";
        state.attributes[i].value = camera.getFeature(i, key);
        state.attributes[i].label = key;
      }
    } catch (const std::exception& err) {
      std::cerr <<"Error getting camera features for status message!\n";
      std::cerr <<err.what() <<"\n";
      state.attributes.resize(0);
    }

    state.n_attributes = state.attributes.size();
    
    if (surveyID.empty()) {
      state.state = STATE_WAITING_FOR_SURVEY;
      status.display = "SURV ID";
      status.status = STATUS_WARN;
      status.message += " needs a survey ID";
    } else if ( camera.isCapturing() ) {
      state.state = STATE_RUNNING;
      status.display = "RUN";
      status.status = STATUS_OK;
      status.message += " is running!";
    } else {
      state.state = STATE_READY;
      status.display = "READY";
      status.status = STATUS_ALERT;
      status.message += " is ready to start!";
    }
  } // if cam connected

  statuses.push_back(status);
}

void CamAcq::CamThread::fillInShutdownStatus(ds_camera_msgs::camera_t &state,
        std::vector<ds_camera_msgs::status_t> &statuses) {
  ds_camera_msgs::status_t status;

  status.utime = getUtimeNow();
  status.key = config->name;
  status.label = config->name;
  status.display = "NO DRV";
  status.status = STATUS_ERROR;
  status.message = "Driver for \"" + config->name + "\" was shut down";
  status.alertOnWarning = false;
  status.alertOnError = false;
  status.errorOnExpire = false;
  status.ttl_seconds = 0; // Just expire immediately
  status.importance = statusImportance;

  statuses.push_back(status);

  // start state message
  state.utime = status.utime;
  state.camID = config->name;
  state.triggerID = config->triggerID;
  state.surveyID = surveyID;
  state.state = STATE_QUIT;
}

void CamAcq::CamThread::startAcq() {
  if (camera.isCapturing() || !camera.isConnected() || surveyID.empty()) {
    std::cout <<"ALREADY CAPTURING!" <<std::endl;
    // we're not ready to start, so don't let it happen!
    return;
  }

  try {
    std::cout <<"CAM: Starting capture..." <<std::endl;
    camera.startCapture();
    camera.startAcquisition();
  } catch (const std::exception& err) {
    std::cerr <<"Unable to start camera capture.  Reason:\n";
    std::cerr <<err.what() <<std::endl;
  }
}

void CamAcq::CamThread::stopAcq() {
  try {
    std::cout <<"CAM: Stopping capture..." <<std::endl;
    camera.stopAcquisition();
    camera.stopCapture();
  } catch (const std::exception& err) {
    std::cerr <<"Unable to start camera capture.  Reason:\n";
    std::cerr <<err.what() <<std::endl;
  }
}

void CamAcq::CamThread::setAttr(const ds_camera_msgs::attribute_t& attr) {
  try {
    if (attr.object == "cam") {
      ROS_WARN_STREAM("setting feature: "<<attr.label<<" to: "<<attr.value);
      camera.setFeature(attr.label, attr.value);
    } else {
      // it's not for the camera, so its for something down the foodchain
      std::unique_lock<std::mutex> lock(attr_mutex);
      new_attrs.push_back(attr);
    }
  } catch (const std::exception& err) {
    ROS_ERROR_STREAM("Unable to set feature \"" <<attr.label << "\" to \"" <<attr.value <<"\"");
    std::cerr <<"Reason: "<<err.what() <<std::endl;
  }
}

void CamAcq::CamThread::frameCallback(const CamAcq::Image::Ptr& img) {

  img->surveyID = surveyID;

  std::unique_lock<std::mutex> lock(attr_mutex);
  img->new_attrs = new_attrs;
  new_attrs.resize(0);
  lock.unlock();
  
  // write the output
  output->push(img);
}



