#pragma once

#include "CameraThread.h"
#include "ProcessingThread.h"
#include "AttributeThread.h"
#include "AbstractOutput.h"

namespace CamAcq {
class CamDriver {
  public:
    CamDriver(const CameraConfig::Ptr& c);
    virtual ~CamDriver();

    // start the driver.  
    void start();
    void stop();

    // stop the driver-- safe to call from interrupt handlers, etc
    void stop_nonblock();

    bool isRunning();

  protected:
    volatile std::atomic_flag running;

    CameraConfig::Ptr config;

    // a list of threads
    CamThread::Ptr camThread;
    ProcessThread::Ptr procThread;
    AttributeThread::Ptr attrThread;
    CamAcq::AbstractOutput::Ptr displayThread; // set during setup, ALSO in outputs

    std::vector<CamAcq::AbstractOutput::Ptr> outputs;
};
};
