#pragma once

#include "CameraConfig.h"
#include "AbstractOutput.h"

/* Output some stuff to the console for every image
 * In general, this isn't super useful, except maybe for 
 * debugging or something */

#define OUTNAME_CONSOLE "console"

namespace CamAcq {
class OutputConsole : public CamAcq::AbstractOutput {
  public:
    //void init() override;
    OutputConsole(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig);
    virtual ~OutputConsole() = default;

    virtual void handleImage(const CamAcq::Image::ConstPtr& img);
    virtual void fillInStatus(ds_camera_msgs::camera_t& state,
            std::vector<ds_camera_msgs::status_t>& statuses) {};
    virtual void fillInShutdownStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {};

  protected:
    using CamAcq::AbstractOutput::input;

};
};
