#include <ros/ros.h>
#include <yaml-cpp/yaml.h>
#include "CamDriver.h"


int main(int argc, char** argv)
{
    ros::init(argc, argv, "ds_camera_node");
    ros::NodeHandle nh;

    ROS_INFO_STREAM("New camera config found");
    CameraConfig::Ptr config(new CameraConfig());

    ros::AsyncSpinner spinner(2);

    ROS_INFO_STREAM("starting cam driver instance w new config");
    CamAcq::CamDriver cam_driver(config);
    spinner.start();

    cam_driver.start(); //only returns when stopped
    cam_driver.stop(); //actually stop


    return 0;
}