#pragma once

#include <string>
#include <jpeglib.h>
#include "AbstractOutput.h"
#include "AVTCamera.h"
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <camera_info_manager/camera_info_manager.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/fill_image.h>
#include "ds_camera_msgs/core_image_t.h"

/* Output an image over ROS
 * Depending on the framerate, this can REALLY slow stuff down, so there's
 * a downsampling option
 */

#define OUTNAME_ROSIMG "rosimg"

namespace CamAcq {
    class OutputRosImage : public CamAcq::AbstractOutput {
    public:
        //void init() override;
        OutputRosImage(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig);
        virtual ~OutputRosImage();

        virtual void handleImage(const CamAcq::Image::ConstPtr& img);
        virtual void fillInStatus(ds_camera_msgs::camera_t& state,
                                  std::vector<ds_camera_msgs::status_t>& statuses) {};
        virtual void fillInShutdownStatus(ds_camera_msgs::camera_t& state,
                                          std::vector<ds_camera_msgs::status_t>& statuses) {};

    protected:
        using CamAcq::AbstractOutput::input;
        const std::string nodeName = ros::this_node::getName();
        CameraConfig::Ptr config;
        AVTCamera camera;
        std::string imgChannel;
        ros::NodeHandle nh;
        image_transport::ImageTransport img_transport_;
        image_transport::Publisher image_pub_;
        bool publishToRos;
    };
};
