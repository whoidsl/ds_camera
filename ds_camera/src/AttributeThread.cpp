#include "AttributeThread.h"


#include <iostream>
#include <cmath>
#include <sys/select.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>


using namespace CamAcq;

AttributeThread::AttributeThread(const CameraConfig::Ptr& cfg) {
  if (!ros::ok()) {
      ROS_BREAK();
    throw std::runtime_error("Unable to start ROS!");
  }
  const auto name = ros::this_node::getName();
  //ros::NodeHandle nh;
  updateInterval = cfg->updateInterval;

  channelState  = cfg->getPublishPath() + "/" + "state";
  channelCmd    = cfg->getPublishPath() + "/" + "cmd";
  channelStatus = cfg->getPublishPath() + "/" + "status";
  surveyID      = cfg->systemName + "/" + cfg->surveyID;

  std::string power_srv = ros::param::param<std::string>("~power_service", "power_cmd");
  power_srv_ = nh.advertiseService<ds_camera_msgs::powerCmd::Request, ds_camera_msgs::powerCmd::Response>
          (name+"/"+power_srv, boost::bind(&AttributeThread::powerCmd, this, _1, _2));
  std::string survey_srv = ros::param::param<std::string>("~survey_service", "survey_cmd");
  survey_srv_ = nh.advertiseService<ds_camera_msgs::surveyCmd::Request, ds_camera_msgs::surveyCmd::Response>
          (name+"/"+survey_srv, boost::bind(&AttributeThread::surveyCmd, this, _1, _2));

  std::string attr_srv = ros::param::param<std::string>("~attribute_service", "attr_cmd");
  attr_srv_ = nh.advertiseService<ds_camera_msgs::attrCmd::Request, ds_camera_msgs::attrCmd::Response>
          (name+"/"+attr_srv, boost::bind(&AttributeThread::attrCmd, this, _1, _2));
  ROS_INFO_STREAM("services set");


  state_msg_pub_ = nh.advertise<ds_camera_msgs::camera_t>(channelState,1000);
  channel_status_pub_ = nh.advertise<ds_camera_msgs::status_t>(channelStatus,1000);
  //channel_cmd_pub_ = nh.advertise<ds_camera_msgs::command_t>(name+"/"+channelCmd,1000);
  ROS_INFO_STREAM("publishers set");

  channel_cmd_sub_ = nh.subscribe(cfg->getPublishPath()+"/" + "cmd", 10, &AttributeThread::handleCmd, this);
  ROS_INFO_STREAM("subscribers set");
}

AttributeThread::~AttributeThread() {
  // TODO
  // Maybe send a "driver shutdown" message?
}

void AttributeThread::threadMain() {
    fd_set readset;
    int rc;
    struct timeval timeout;
    double intpart, fractpart;
    fractpart = modf(updateInterval, &intpart);
    timeout.tv_sec = static_cast<int>(intpart + 1.0e-4); // add a little extra for round-off error
    timeout.tv_usec = static_cast<int>(1.0e6 * fractpart);

    ros::Rate r(1.0);

    while (running.test_and_set()) {
      // send a status message
      sendStatus();
      r.sleep();
      // tv gets reduced by the amount waited for each select call,
      // so just keep making copies
      struct timeval tv;
      memcpy(&tv, &timeout, sizeof(struct timeval));

    } // while running

  sendShutdownStatus();
}

bool AttributeThread::powerCmd(ds_camera_msgs::powerCmd::Request& req, ds_camera_msgs::powerCmd::Response& res) {
    ROS_ERROR_STREAM("called powercmd service");
    switch (req.power) {
        case ds_camera_msgs::powerCmd::Request::CAM_START:
            ROS_ERROR_STREAM("calling start acq thread");
            acqThread->startAcq();
            res.action = "commanded camera start";
            break;
        case ds_camera_msgs::powerCmd::Request::CAM_STOP:
            acqThread->stopAcq();
            res.action = "commanded camera stop";
            break;
        default:
            ROS_WARN_STREAM("Unrecognized camera start/stop cmd");
    }
    return true;
}

bool AttributeThread::surveyCmd(ds_camera_msgs::surveyCmd::Request& req, ds_camera_msgs::surveyCmd::Response& res) {
    ROS_ERROR_STREAM("calling set survey ID");
    acqThread->setSurveyID(req.survey_name);
    res.command_sent = req.survey_name;
    return true;
}


bool AttributeThread::attrCmd(ds_camera_msgs::attrCmd::Request& req, ds_camera_msgs::attrCmd::Response& res) {
    ds_camera_msgs::attribute_t attr;
    attr.object  = req.attr_object;
    attr.label = req.attr_label;
    attr.value = req.attr_value;
    acqThread->setAttr(attr);
    res.command_sent = "sent commanded attributes";
    return true;
}

void AttributeThread::handleCmd(const ds_camera_msgs::command_t& msg) {
  // start/stop commands
  std::vector<std::string> args;
  if (msg.command == "START") {
    acqThread->startAcq();
  } else if (msg.command == "STOP") {
    acqThread->stopAcq();
  } else if (msg.command == "SURVID") {
    if (msg.num_args < 1) {
      std::cerr <<"SURVID command had too few arguments!\n";
    } else {
      acqThread->setSurveyID(msg.args[0]);
    }
  } else if (msg.command == "ATTR") {
    if (msg.num_args < 3) {
      std::cerr <<"ATTR command had too few arguments!\n";
    } else {
      ds_camera_msgs::attribute_t attr;
      attr.object = msg.args[0];
      attr.label  = msg.args[1];
      attr.value  = msg.args[2];

      acqThread->setAttr(attr);
    }
  } else {
    std::cerr <<"Got UNKNOWN Camera Driver Command \"" <<msg.command <<"\"\n";
  }
}

void AttributeThread::sendStatus() {
  // have all the other threads fill in their status messages
  ds_camera_msgs::camera_t stateMsg;
  std::vector<ds_camera_msgs::status_t> statusMsgs;
  for (auto iter=threads.begin(); iter != threads.end(); iter++) {
    (*iter)->fillInStatus(stateMsg, statusMsgs);
  }
  stateMsg.n_attributes = stateMsg.attributes.size();
  state_msg_pub_.publish(stateMsg);
  for (auto iter=statusMsgs.begin(); iter != statusMsgs.end(); iter++) {
    channel_status_pub_.publish((*iter));
  }
}

void AttributeThread::sendShutdownStatus() {
  // have all the other threads fill in their status messages
  ds_camera_msgs::camera_t stateMsg;
  std::vector<ds_camera_msgs::status_t> statusMsgs;

  for (auto iter=threads.begin(); iter != threads.end(); iter++) {
    (*iter)->fillInShutdownStatus(stateMsg, statusMsgs);
  }
  stateMsg.n_attributes = stateMsg.attributes.size();

  // actually send
  state_msg_pub_.publish(stateMsg);
  for (auto iter=statusMsgs.begin(); iter != statusMsgs.end(); iter++) {
    channel_status_pub_.publish((*iter));
  }
}
