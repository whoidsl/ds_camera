
#include "DriverQueue.h"

#include <iostream>
#include <chrono>

template<typename T>
CamAcq::DriverQueue<T>::DriverQueue(const std::string& n) {
  init(64, n);
}

template<typename T>
CamAcq::DriverQueue<T>::DriverQueue(size_t space, const std::string& n) {
  init(space, n);
}

template<typename T>
CamAcq::DriverQueue<T>::~DriverQueue() {
  // do nothing (?)
}

template<typename T>
void CamAcq::DriverQueue<T>::push(const T& newElement) {

  std::unique_lock<std::mutex> lock(lock_m); // unlocks on destruction

  if (count < buffer.size()) {
    buffer[writeIdx] = newElement;
    writeIdx = (writeIdx + 1) % buffer.size();
    count++;
    count_cv.notify_one();

  } else {
    if (throwOnOverflow) {
      throw DriverQueueOverflowException(name);
    } else {
      std::cerr <<"WARNING! DriverQueue " <<name <<" OVERFLOW!" <<std::endl;
    }
  }

}

template<typename T>
bool CamAcq::DriverQueue<T>::pop(T& ret) {
  std::unique_lock<std::mutex> lock(lock_m); // unlocks on destruction

  // only wait if there are no samples
  while (count < 1 && noWait == false) {
    count_cv.wait(lock);
  }
  if (count < 1) {
    return false;
  }

  // there should be a sample at readIdx now
  ret = buffer[readIdx];
  buffer[readIdx] = T();
  readIdx = (readIdx + 1)  % buffer.size();
  count--;

  return true;
}

template <typename T>
bool CamAcq::DriverQueue<T>::pop_timeout(T& ret, int64_t maxMilliseconds) {
  std::unique_lock<std::mutex> lock(lock_m); // unlocks on destruction

  if (!count_cv.wait_for(lock, std::chrono::milliseconds(maxMilliseconds),
          [this](){ return this->count > 0 || this->noWait == true; })) {
    // timed out
    return false;
  }
  // finished!
  if (count < 1) {
    return false;
  }

  // there should be a sample at readIdx now
  ret = buffer[readIdx];
  buffer[readIdx] = T();
  readIdx = (readIdx + 1)  % buffer.size();
  count--;

  return true;
}

template<typename T>
void CamAcq::DriverQueue<T>::stopWaiting() {
  noWait = true;
  count_cv.notify_all();
}

template<typename T>
void CamAcq::DriverQueue<T>::init(size_t space, const std::string& n) {
  name = n;

  throwOnOverflow=false;
  noWait=false;

  readIdx=0;
  writeIdx=0;

  count=0;
  buffer.resize(space);
}

