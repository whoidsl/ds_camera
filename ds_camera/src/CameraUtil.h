#pragma once

#include <string>
#include <VimbaCPP/Include/VimbaCPP.h>

std::string errorCodeToString(const VmbErrorType& err);
bool checkErrorCode(const VmbErrorType& err, bool throwException=true);

int64_t getUtimeNow();
std::string getIconPath();
