#include "DriverImage.h"
#include "DriverQueue_impl.hpp"

template class CamAcq::DriverQueue<CamAcq::Image::Ptr>;
template class CamAcq::DriverQueue<CamAcq::Image::ConstPtr>;
