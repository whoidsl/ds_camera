#pragma once

#include "DriverThread.h"
#include "DriverImage.h"
#include "DriverQueue.h"
#include "CameraConfig.h"
#include <yaml-cpp/yaml.h>
#include <ros/ros.h>

namespace CamAcq {

class AbstractOutput : public CamAcq::Thread {
  public:
    AbstractOutput();
    //virtual void init();
    AbstractOutput(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig);
    virtual ~AbstractOutput();

    typedef std::shared_ptr<AbstractOutput> Ptr;
    typedef std::shared_ptr<const AbstractOutput> ConstPtr;


    virtual void threadMain();
    virtual void handleImage(const CamAcq::Image::ConstPtr& img)=0;
    virtual bool isDisplayOutput() const { return false; };

    typedef CamAcq::DriverQueue<CamAcq::Image::ConstPtr> InputQueue_t;
    const InputQueue_t::Ptr& getInput() const { return input; };


  protected:
    InputQueue_t::Ptr input;


};
};
