#include "AbstractOutput.h"

#include <iostream>

using namespace CamAcq;

AbstractOutput::AbstractOutput(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig) {

  int queueSize = 64;
  std::string queueName;
  for (auto elem : outputConfig) {
      if (elem.first == "queueSize") {
          queueSize = static_cast<int>(elem.second);
      }
      if (elem.first == "queueName") {
          queueName = static_cast<std::string>("name");
      }
  }

  input.reset( new typename AbstractOutput::InputQueue_t(queueSize, queueName) );
}

AbstractOutput::~AbstractOutput() {
  stop();
}

void AbstractOutput::threadMain() {
  if (!input) {
    throw std::logic_error("MUST connect input before running thread");
  }

  // make sure we stop
  bool valid;
  Image::ConstPtr img;
  while (running.test_and_set()) {

    // sometimes pop returns without a valid image (timeout, etc)
    if (! input->pop(img) ) {
      continue;
    }

    // handle image
    handleImage(img);
  }
  running.clear();
}
