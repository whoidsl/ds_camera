#pragma once

#include <map>
#include <list>
#include <atomic>

#include "CameraConfig.h"
#include "DriverObject.h"
#include "DriverImage.h"

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <camera_info_manager/camera_info_manager.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/fill_image.h>

#include <VimbaCPP/Include/VimbaCPP.h>

/* 
 * Class for an AVT GigE Camera
 * Based on the shiny new Vimba API
 */

// forward declaration
class CamObserver;
class FrameObserver;
class EventObserver;
class FrameFiredObserver;

class AVTCamera {
  friend class CamObserver;
  friend class FrameObserver;
  friend class EventObserver;
  friend class FrameFiredObserver;
  public:
    AVTCamera(const CameraConfig::Ptr& cfg);
    virtual ~AVTCamera();

    ros::NodeHandle nh;
    image_transport::ImageTransport it_;
    image_transport::Publisher img_pub_;

    // Internal camera operations
    void initAPI(); // allows to be called AFTER initialization
    void connect();
    void disconnect();

    void startCapture();     // start
    void startAcquisition(); // pause
    void stopAcquisition();  // unpause
    void stopCapture();      // stop

    bool isConnected() const { return plugged && ready; };
    bool isPlugged()   const { return plugged; };
    bool isCapturing() const { return capturing; };
    
    typedef boost::function<void (const CamAcq::Image::Ptr&)> FrameReady_t;
    void setOnFrameReady(FrameReady_t callback);
    
    std::string getFeature(const std::string& key) const;
    std::string getFeature(size_t idx, std::string& key) const;
    void setFeature(const std::string& key, const std::string& value);
    void setFeature(size_t idx, const std::string& value);
    size_t numFeatures() const { return features.size(); };


    void printFeatures();
    
    const std::string& getUid() const { return config->getUid(); };

    bool isCorrectCam(const AVT::VmbAPI::CameraPtr& pCam);

    CamAcq::Image::Ptr getNextImage(); // blocks until next image is ready

  protected:
    // ////////////////////////////////////////////////////////////////////////
    // Core / Setup / Shutdown
    // ////////////////////////////////////////////////////////////////////////
    CameraConfig::Ptr config;
    size_t numFrames;
    AVT::VmbAPI::CameraPtr handle;
    VmbInt64_t payloadSize;
    std::atomic_bool ready, plugged, capturing, startCaptureCalled;
    std::atomic_flag continueOldFile;
    const std::string nodeName = ros::this_node::getName();
    bool isVimba;

    // we're going to use the time of the last frame to 
    // recover a clock offset between the camera and the 
    // host computer.  At any given time, this will probably be the time of 
    // the image being handled, but even if we can't guarantee that, '
    // it'll be really close!
    std::mutex cameraTimeMutex;
    int64_t lastFrameClockOffset;
    int64_t cameraTimestampFreqHz;

    void initCapture(size_t n_frames);
    void loadInitialConfig();

    // set the clock offset based on a frame time and its
    // event trigger
    void frameTriggerTime(VmbInt64_t cameraTime, int64_t computerTime);

    // return the current offset between the camera clock 
    int64_t getClockOffset();

    // ////////////////////////////////////////////////////////////////////////
    // features (attributes)
    // ////////////////////////////////////////////////////////////////////////

    typedef std::map<std::string, AVT::VmbAPI::FeaturePtr> FeatureMap;
    AVT::VmbAPI::FeaturePtrVector features; // camera attributes
    FeatureMap featureTable;

    VmbErrorType AdjustPacketSize(const AVT::VmbAPI::CameraPtr& _handle);

    void loadFeatures();
    void printCameraDetails(const AVT::VmbAPI::CameraPtr& cam);
    std::string featureToString(const AVT::VmbAPI::FeaturePtr& f) const;
    void setFeature_inner(const AVT::VmbAPI::FeaturePtr& f, const std::string& value);

    void initFeatureObservers();
 
    // ////////////////////////////////////////////////////////////////////////
    // frame management
    // ////////////////////////////////////////////////////////////////////////
    CamAcq::DriverObjectPool<CamAcq::Image> img_pool; // ALL frames
    std::map <AVT::VmbAPI::Frame*, CamAcq::Image::Ptr> img_queue; // Frames waiting for image acq
    std::mutex img_queue_mutex;
    FrameReady_t frameReadyCB;

    AVT::VmbAPI::FramePtrVector frames;
    AVT::VmbAPI::IFrameObserverPtr frame_observer;
    
    void announceImage_nolock(const CamAcq::Image::Ptr& img);
    void  requeueImage_nolock(const CamAcq::Image::Ptr& img);
    void garbageCollectImages_nolock();
    void handleFrame(const AVT::VmbAPI::FramePtr& frame);

};
