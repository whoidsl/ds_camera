#include <iostream>
#include <signal.h>
#include <unistd.h>

#include <ros/ros.h>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <yaml-cpp/yaml.h>

#include "CamDriver.h"

struct CmdLineArgs {
  std::string cameraConfig;
  std::string cameraName;

  bool verbose;
};
/*
int parseArgs(CmdLineArgs& args, int argc, char ** argv) {
  args.cameraConfig = "";
  args.cameraName = "";
  bool verbose = false;

  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()
    ("help,h", "Print help messages")
    ("config,c", po::value<std::string>(&args.cameraConfig)->required(), "Camera config file")
    ("cam,C", po::value<std::string>(&args.cameraName)->required(), "Camera entry in config file to use")
    ("verbose,v", "extra output messages");

  po::variables_map vm;
  try {
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout <<desc <<"\n";
      return 1;
    }

    if (vm.count("verbose")) {
      args.verbose = true;
    }
  } catch (po::error& e) {
    std::cerr <<"ERROR: " <<e.what() <<"\n\n" <<desc <<std::endl;
    return -1;
  }

  return 0;
}

CamAcq::CamDriver* driver = NULL;
void signal_handler(int signum) {
  std::cout <<"\n\nGot CTRL-C, attempting to quit...\n\n" <<std::endl;
  if (driver) {
    driver->stop_nonblock();
  }
}
*/
int main(int argc, char** argv) {
  CmdLineArgs args;
  /*
  int rc;
  if (rc = parseArgs(args, argc, argv)) {
    if (rc > 0) {
      return 0;
    }
    std::cerr <<"\n\nError parsing options, quitting...\n\n" <<std::endl;
    return rc;
  }
*/
  //Options parsed successfully, start ros and remap the name
  ros::init(argc, argv, "sentry_cam");
  ros::NodeHandle nh;

  // ok, options parsed successfully
  if (args.verbose) {
    std::cout <<"\nLoading config from \"" <<args.cameraConfig <<"\"\n\n";
  }

  // TODO: Add support for signals and stuff
  //CameraConfig::Ptr config(new CameraConfig(args.cameraConfig, args.cameraName));
  //CameraConfig::Ptr config(new CameraConfig("/home/ivandor/catkin_ws/src/ds_camera/ds_camera/config/ian_test.yaml", "camA"));
  CameraConfig::Ptr config(new CameraConfig());
/*
  struct sigaction int_action;
  sigemptyset(&int_action.sa_mask);
  int_action.sa_flags = 0;
  int_action.sa_handler = signal_handler;
  if (sigaction(SIGINT, &int_action, NULL)) {
    std::cerr <<"Unable to register quit callback, quitting!" <<std::endl;
  }
*/

    ros::AsyncSpinner spinner(0);
    CamAcq::CamDriver cam_driver(config);
    spinner.start();



  //driver = &cam_driver;
  cam_driver.start();


/*
  if (args.verbose) {
    std::cout <<"Starting driver for camera \"" <<config->getName();
    std::cout <<"\" with UID=\"" <<config->getUid() <<"\"" <<std::endl;
  }
  */
  //cam_driver.start();
  /*
  if (args.verbose) {
    std::cout <<"Quitting cleanly..." <<std::endl;
  }
  cam_driver.stop();
  if (args.verbose) {
    std::cout <<"Driver quit!" <<std::endl;
  }
  */
  return 0;
}

