#include "CameraConfig.h"

#include <stdexcept>
#include <iostream>
#include <unistd.h>
#include <xmlrpcpp/XmlRpcException.h>



CameraConfig::CameraConfig() {
    const auto name = ros::this_node::getName();
    //Setting up a dynamic reconfigure server
    dr_srv_.reset(new dynamic_reconfigure::Server<ds_camera::AvtVimbaCameraConfig>(ros::NodeHandle("~")));
    cb_ = boost::bind(&CameraConfig::dynamicParamCallback, this, _1, _2);
    dr_srv_->setCallback(cb_);
    ROS_INFO_STREAM("dynamic parameter server set");

    loadConfig();

  cmd_msg_pub_ = nh.advertise<ds_camera_msgs::command_t>(publishPath+"/" + "cmd",1000);

}

void CameraConfig::setupParameters() {
    nh.getParam(nodeName+"/"+"uid", uid);
    nh.getParam(nodeName+"/"+"vimba_api_check", vimbaApi);
    //ROS_ERROR_STREAM("camera uid: "<< static_cast<std::string>(uid));
    nh.getParam(nodeName+"/"+"survey_id", surveyID);
    nh.getParam(nodeName+"/"+"acquisition_mode", acquisitionMode);
    nh.getParam(nodeName+"/"+"acquisition_rate", acquisitionFrameRateAbs);
    nh.getParam(nodeName+"/"+"trigger_source", triggerSource);
    nh.getParam(nodeName+"/"+"trigger_mode", triggerMode);
    nh.getParam(nodeName+"/"+"exposure_auto", exposureAuto);
    nh.getParam(nodeName+"/"+"exposure_auto_alg", exposureAutoAlg);
    nh.getParam(nodeName+"/"+"exposure_auto_tol", exposureAutoTol);
    nh.getParam(nodeName+"/"+"exposure_auto_max", exposureAutoMax);
    nh.getParam(nodeName+"/"+"exposure_auto_target", exposureAutoTarget);
    nh.getParam(nodeName+"/"+"gain_auto", gainAuto);
    nh.getParam(nodeName+"/"+"gain", gain);
    nh.getParam(nodeName+"/"+"height", height);
    nh.getParam(nodeName+"/"+"width", width);
    nh.getParam(nodeName+"/"+"pixel_format", pixelFormat);
    nh.getParam(nodeName+"/"+"stream_bytes_per_second", streamBytesPerSecond);
    nh.getParam(nodeName+"/"+"binning_x", binningHorizontal);
    nh.getParam(nodeName+"/"+"bandwidth_control_mode", bandwidthControlMode);
    nh.getParam(nodeName+"/"+"strobe_delay", strobeDelay);
    nh.getParam(nodeName+"/"+"strobe_duration", strobeDuration);
    nh.getParam(nodeName+"/"+"strobe_duration_mode", strobeDurationMode);
    nh.getParam(nodeName+"/"+"strobe_source", strobeSource);
    nh.getParam(nodeName+"/"+"sync_out_polarity", syncOutPolarity);
    nh.getParam(nodeName+"/"+"sync_out_selector", syncOutSelector);
    nh.getParam(nodeName+"/"+"sync_out_source", syncOutSource);
}

void CameraConfig::loadConfig() {
    publishPath = nodeName + "/"+ systemName;

    //Check the outputs and pass to rest of driver
    XmlRpc::XmlRpcValue output_params;
    if (!nh.getParam(nodeName + "/" + "outputs", output_params)) {
        ROS_ERROR_STREAM("NO OUTPUTS SPECIFIED");
    }
    for (size_t i = 0; i < output_params.size(); i++) {
        //ROS_ERROR_STREAM(output_params[i]);
    }
    //ROS_WARN_STREAM("outtype: " << output_params.getType());
    if (output_params.getType() != XmlRpc::XmlRpcValue::TypeArray) {
        ROS_ERROR_STREAM("Outputs must be a map");
    }
    for (size_t i = 0; i < output_params.size(); i++) {
        std::string output_name;
        for (auto elem : output_params[i]) {
            if (elem.first == "name") {
                output_name = static_cast<std::string>(elem.second);
            }
        }
        ROS_INFO_STREAM("Loading output: " << output_name);
    }
    outputParams = output_params;
}

std::string CameraConfig::getSurveyID() {
    if (!nh.getParam(nodeName + "/" + "survey_id", surveyID)) {
        surveyID = "default_survey";
    } else {
        nh.getParam(nodeName + "/" + "survey_id", surveyID);
    }
    return surveyID;
}

float CameraConfig::getUpdateInterval() {
    if (!nh.getParam(nodeName+"/"+"update_interval", updateInterval)) {
        updateInterval = 1.0f;
    }
    else {
        nh.getParam(nodeName+"/"+"update_interval", updateInterval);
    }
    return updateInterval;
}

size_t CameraConfig::getNumFrames() {
    if (!nh.getParam(nodeName+"/"+"num_frame_buffers", numFrames)) {
        numFrames = 32; //default value
    }
    else {
        nh.getParam(nodeName+"/"+"num_frame_buffers", numFrames);
    }
    return numFrames;
}

/** Dynamic reconfigure callback
*
*  Called immediately when callback first defined. Called again
*  when dynamic reconfigure starts or changes a parameter value.
*
*  @param newconfig new Config values
*  @param level bit-wise OR of reconfiguration levels for all
*               changed parameters (0xffffffff on initial call)
**/

void CameraConfig::dynamicParamCallback(ds_camera::AvtVimbaCameraConfig &config, uint32_t level) {
    setupParameters();
    try {
        // resolve frame ID using tf_prefix parameter
        if (config.frame_id == "") {
            config.frame_id = "camera";
        }
        systemName = config.system_name;
        //surveyID = "test_survey";
        frameID = config.frame_id;
        logToDisk = config.log_to_disk;
        publishToRos = config.ros_img_publish;
        trigTimestampTopic = config.trig_timestamp_topic;

        if (!vimbaApi) {
            if (gainAuto != config.gain_auto) {
                gainAuto = config.gain_auto;
                updateAttrConfig("GainSelector", "All");
            }
            if (gain != config.gain) {
                gain = config.gain;
                updateAttrConfig("GainRaw", std::to_string(config.gain));
            }
         }

        if (acquisitionCmd != config.acquisition_cmd) {
            acquisitionCmd = config.acquisition_cmd;
            if (config.acquisition_cmd != 0) {
                cmd.command = "START";
                cmd.args.clear();
                cmd.num_args = 0;
                cmd_msg_pub_.publish(cmd);
            }
            else {
                cmd.command = "STOP";
                cmd.args.clear();
                cmd.num_args = 0;
                cmd_msg_pub_.publish(cmd);
            }
        }
        if (config.survey_id != surveyID) {
            surveyID = config.survey_id;
            cmd.command = "SURVID";
            cmd.num_args = 1;
            cmd.args.clear();
            cmd.args.push_back(config.survey_id);
            cmd_msg_pub_.publish(cmd);
        }
        if (acquisitionMode != config.acquisition_mode) {
            acquisitionMode = config.acquisition_mode;
            updateAttrConfig("AcquisitionMode", config.acquisition_mode);
        }
        if (acquisitionFrameRateAbs != config.acquisition_rate) {
            acquisitionFrameRateAbs = config.acquisition_rate;
            updateAttrConfig("AcquisitionFrameRateAbs", std::to_string(config.acquisition_rate));
        }
        if (triggerMode != config.trigger_mode) {
            triggerMode = config.trigger_mode;
            updateAttrConfig("TriggerMode", config.trigger_mode);
        }
        if (exposureAuto != config.exposure_auto) {
            exposureAuto = config.exposure_auto;
            updateAttrConfig("ExposureAuto", config.exposure_auto);
        }
        if (exposureAutoAlg != config.exposure_auto_alg) {
            exposureAutoAlg = config.exposure_auto_alg;
            updateAttrConfig("ExposureAutoAlg", config.exposure_auto_alg);
        }
        if (exposureAutoTol != config.exposure_auto_tol) {
            exposureAutoTol = config.exposure_auto_tol;
            updateAttrConfig("ExposureAutoAdjustTol", std::to_string(config.exposure_auto_tol));
        }
        if (exposureAutoMax != config.exposure_auto_max) {
            exposureAutoMax = config.exposure_auto_max;
            updateAttrConfig("ExposureAutoMax", std::to_string(config.exposure_auto_max));
        }
        if (exposureAutoTarget != config.exposure_auto_target) {
            exposureAutoTarget = config.exposure_auto_target;
            updateAttrConfig("ExposureAutoTarget", std::to_string(config.exposure_auto_target));
        }
        if (gainAuto != config.gain_auto) {
            gainAuto = config.gain_auto;
            updateAttrConfig("GainAuto", config.gain_auto);
        }
        if (gain != config.gain) {
            gain = config.gain;
            updateAttrConfig("Gain", std::to_string(config.gain));
        }
        if (height != config.height) {
            height = config.height;
            updateAttrConfig("Height", std::to_string(config.height));
        }
        if (width != config.width) {
            width = config.width;
            updateAttrConfig("Width", std::to_string(config.width));
        }
        if (pixelFormat != config.pixel_format) {
            pixelFormat = config.pixel_format;
            updateAttrConfig("PixelFormat", config.pixel_format);
        }
        if (bandwidthControlMode != config.bandwidth_control_mode) {
            bandwidthControlMode = config.bandwidth_control_mode;
            updateAttrConfig("BandwidthControlMode", config.bandwidth_control_mode);
        }
        if (streamBytesPerSecond != config.stream_bytes_per_second) {
            streamBytesPerSecond = config.stream_bytes_per_second;
            updateAttrConfig("StreamBytesPerSecond", std::to_string(config.stream_bytes_per_second));
        }
        if (binningHorizontal != config.binning_x) {
            binningHorizontal = config.binning_x;
            updateAttrConfig("BinningHorizontal", std::to_string(config.binning_x));
        }
        if (strobeDelay != config.strobe_delay) {
            strobeDelay = config.strobe_delay;
            updateAttrConfig("StrobeDelay", std::to_string(config.strobe_delay));
        }
        if (strobeDuration != config.strobe_duration) {
            strobeDuration = config.strobe_duration;
            updateAttrConfig("StrobeDuration", std::to_string(config.strobe_duration));
        }
        if (strobeDurationMode != config.strobe_duration_mode) {
            strobeDurationMode = config.strobe_duration_mode;
            updateAttrConfig("StrobeDurationMode", config.strobe_duration_mode);
        }
        if (strobeSource != config.strobe_source) {
            strobeSource = config.strobe_source;
            updateAttrConfig("StrobeSource", config.strobe_source);
        }
        if (syncOutPolarity != config.sync_out_polarity) {
            syncOutPolarity = config.sync_out_polarity;
            updateAttrConfig("SyncOutPolarity", config.sync_out_polarity);
        }
        if (syncOutSelector != config.sync_out_selector) {
            syncOutSelector = config.sync_out_selector;
            updateAttrConfig("SyncOutSelector", config.sync_out_selector);
        }
        if (syncOutSource != config.sync_out_source) {
            syncOutSource = config.sync_out_source;
            updateAttrConfig("SyncOutSource", config.sync_out_source);
        }
    } catch (const std::exception& e) {
        ROS_ERROR_STREAM("Error reconfiguring camera node : " << e.what());
    }
}

void CameraConfig::updateAttrConfig(std::string param_name, std::string param_value) {
    //ROS_WARN_STREAM("calling attr config");
    cmd.command = "ATTR";
    cmd.num_args = 3;
    cmd.args.clear();
    cmd.args.push_back("cam");
    cmd.args.push_back(param_name);
    cmd.args.push_back(param_value);
    //ROS_ERROR_STREAM("attr cmd: "<<cmd);
    cmd_msg_pub_.publish(cmd);
}
