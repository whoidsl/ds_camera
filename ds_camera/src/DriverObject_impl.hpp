#include "DriverObject.h"

#include <iostream>

///////////////////////////////////////////////////////////////////////////////
// DriverObject-- object passed through the driver chain
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// DriverObjectPool-- manages driver objects
///////////////////////////////////////////////////////////////////////////////

template <typename T>
CamAcq::DriverObjectPool<T>::DriverObjectPool() {
  // do nothing
}

template <typename T>
CamAcq::DriverObjectPool<T>::~DriverObjectPool() {
  //std::cout <<"Destroying DriverObject pool! This WILL break things unless we're quitting!" <<std::endl;
  
  // do nothing
}


template <typename T>
void CamAcq::DriverObjectPool<T>::preallocate(size_t number, Allocator_t allocator) {
  ulock(myLock);

  size_t start = objs.size();
  objs.resize(objs.size() + number);

  for (size_t i=0; i<number; i++) {
    objs[start+i].reset(allocator());
  }
  // auto-unlocks!
}

template <typename T>
size_t CamAcq::DriverObjectPool<T>::getCapacity() const {
  ulock(myLock);
  return objs.size();
  // auto-unlocks!
}

template <typename T>
size_t CamAcq::DriverObjectPool<T>::getFreeCapacity() const {
  ulock(myLock);
  size_t ret = 0;

  for (typename ObjVector::const_iterator iter = objs.begin();
      iter != objs.end(); iter++) {
    if (iter->unique()) {
      ret++;
    }
  }

  return ret;
  // auto-unlocks!
}

template <typename T>
typename CamAcq::DriverObjectPool<T>::ObjPtr CamAcq::DriverObjectPool<T>::alloc() {

  ulock(myLock);
  // just return the first unallocated object!
  for (typename ObjVector::const_iterator iter = objs.begin();
      iter != objs.end(); iter++) {
    if (iter->unique()) {
      return *iter;
    }
  }

}

template <typename T>
void CamAcq::DriverObjectPool<T>::mapFreeObjects(typename CamAcq::DriverObjectPool<T>::RegisterCallback_t callback) {
  ulock(myLock);


  for (typename ObjVector::const_iterator iter = objs.begin();
      iter != objs.end(); iter++) {
    if (iter->unique()) {
      callback(*iter);
    }
  }
}

template <typename T>
void CamAcq::DriverObjectPool<T>::clear() {
  ulock(myLock);

  objs.clear();
}
