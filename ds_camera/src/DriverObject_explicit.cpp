#include "DriverImage.h"
#include "DriverObject_impl.hpp"


// make these things get compiled here, so we aren't constantly 
// re-compiling them
template class CamAcq::DriverObjectPool<CamAcq::Image>;
