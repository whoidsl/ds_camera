
#pragma once

/*
 * A producer / consumer queue to use between driver threads
 */

#include <boost/noncopyable.hpp>
#include <string>
#include <vector>
#include <stdexcept>
#include <mutex>
#include <condition_variable>

namespace CamAcq {
  
class DriverQueueOverflowException : public std::runtime_error {
  public:
    explicit DriverQueueOverflowException(const std::string& what_arg) : std::runtime_error(what_arg) {};
};

template<typename T>
class DriverQueue : private boost::noncopyable {
  public:
    typedef std::shared_ptr<DriverQueue<T> > Ptr;
    typedef std::shared_ptr<const DriverQueue<T> > ConstPtr;

    DriverQueue(const std::string& n); // default to a 64-element Queue
    DriverQueue(size_t space, const std::string& n);
    virtual ~DriverQueue();

    void push(const T& newElement);
    bool pop(T& ret); // bool true if ret is valid, false otherwise
    bool pop_timeout(T& ret, int64_t maxMilliseconds);
    void stopWaiting(); // stop any process waiting on the output of this queue

    const std::string& getName() const { return name; };
    void setName(const std::string& n) { name = n; };

    size_t capacity() const { return buffer.size(); };
    size_t size() const;

  protected:
    std::string name;

    bool throwOnOverflow;
    volatile bool noWait;

    size_t readIdx;
    size_t writeIdx;

    size_t count;
    std::vector<T> buffer;

    std::mutex lock_m;
    std::condition_variable count_cv;

    void init(size_t space, const std::string& n);
};
};
