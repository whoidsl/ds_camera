#pragma once

#include <string>
#include <boost/filesystem.hpp>
#include <tiffio.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <camera_info_manager/camera_info_manager.h>
#include <image_transport/image_transport.h>
#include "ds_camera_msgs/image_info_t.h"
#include "AbstractOutput.h"

#define OUTNAME_TIFF "tiff"

namespace CamAcq {
class OutputTiff : public CamAcq::AbstractOutput {
  public:
    //void init() override;
    OutputTiff(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig);
    virtual ~OutputTiff();

    virtual void handleImage(const CamAcq::Image::ConstPtr& img);
    virtual void fillInStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses);
    virtual void fillInShutdownStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {}; // do nothing


  protected:
    ros::NodeHandle nh;
    const std::string nodeName = ros::this_node::getName();
    ros::Publisher tiff_info_pub_;

    // overhead
    std::string name;
    std::string systemName;
    std::string camName;
    boost::filesystem::path root;
    std::string currentSurveyId;
    boost::filesystem::path outputDir;
    int spaceStatus;
    double statusImportance;
    float statusTtlSeconds;
    size_t freeMbytesError, freeMbytesWarn;

    // actual tiff-writing stuff
    TIFF* outfile;
    size_t outfileImages;
    size_t imagesPerFile;

    uint16_t compression;

    // metadata
    std::string artist;
    std::string hostcomputer;
    std::string copyright;
    

    // inner functions
    std::string makeFilename(std::string& systemName, std::string& isodatetime, int64_t utime, int64_t frameNumber) const;
    //std::string makeFilename(int64_t utime, int64_t frameNumber, std::string& isodatetime) const;

};
};
