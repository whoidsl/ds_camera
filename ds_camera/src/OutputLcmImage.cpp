#include "OutputLcmImage.h"

#include <iostream>
#include <iomanip>

#include "CameraUtil.h"

#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_UYVY 1498831189
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_YUYV 1448695129
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_IYU1 827677001
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_IYU2 844454217
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_YUV420 842093913
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_YUV411P 1345401140
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_I420 808596553
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_NV12 842094158
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY 1497715271
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB 859981650
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BGR 861030210
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGBA 876758866
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BGRA 877807426
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BAYER_BGGR 825770306
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BAYER_GBRG 844650584
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BAYER_GRBG 861427800
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BAYER_RGGB 878205016
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BE_BAYER16_BGGR 826360386
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BE_BAYER16_GBRG 843137602
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BE_BAYER16_GRBG 859914818
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BE_BAYER16_RGGB 876692034
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_LE_BAYER16_BGGR 826360396
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_LE_BAYER16_GBRG 843137612
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_LE_BAYER16_GRBG 859914828
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_LE_BAYER16_RGGB 876692044
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG 1196444237
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BE_GRAY16 357
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_LE_GRAY16 909199180
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BE_RGB16 358
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_LE_RGB16 1279412050
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BE_SIGNED_GRAY16 359
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_BE_SIGNED_RGB16 360
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_FLOAT_GRAY32 842221382
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_INVALID -2
#define BOT_CORE_IMAGE_T_PIXEL_FORMAT_ANY -1

using namespace CamAcq;

OutputLcmImage::OutputLcmImage(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig) : AbstractOutput(driverConfig, outputConfig) {
    std::string subchannel;
    for (auto elem : outputConfig) {
        if (elem.first == "subchannel") {
            subchannel = static_cast<std::string>(elem.second);
        }
    }
    imgChannel = driverConfig->getPublishPath()
    + "/" + subchannel;
  //std::cout <<"Using LCM Path " <<imgChannel <<"\n";

  // downsampling options
  downsample = 0; // disable downsampling
  droppedToDownsample = 0;
  for (auto elem : outputConfig) {
      if (elem.first == "downsample") {
          downsample = static_cast<int>(elem.second);
          ROS_INFO_STREAM("Camera only LCM-ing every "<<downsample<<" samples!\n");
      }
  }

  //  jpeg options
  useJpeg = false;
  jpegQuality = -1;
  for (auto elem : outputConfig) {
      if (elem.first == "jpegQuality") {
          useJpeg = true;
          jpegQuality = static_cast<int>(elem.second);
      }
  }
  for (auto elem : outputConfig) {
      if (elem.first == "useJpeg") {
          useJpeg = static_cast<bool>(elem.second);
      }
  }

  ros::NodeHandle nh;
  ros::Publisher image_pub_ = nh.advertise<ds_camera_msgs::core_image_t>("image_compressed", 1);
  imgBuf = NULL;
  imgBufSize = 0;

  jpegBuf = NULL;
  jpegBufLen = 0;
}

OutputLcmImage::~OutputLcmImage() {

  if (imgBuf) {
    free(imgBuf);
    imgBuf = NULL;
    imgBufSize = 0;
  }

  if (jpegBuf) {
    free(jpegBuf);
    jpegBuf = NULL;
    jpegBufLen = 0;
  }
}

void OutputLcmImage::handleImage(const CamAcq::Image::ConstPtr& img) {

  if (downsample > 1) {
    droppedToDownsample++;
    if (droppedToDownsample < downsample) {
      //std::cout <<"Dropping image " <<droppedToDownsample <<"due to downsampling...\n";
      return;
    }
    droppedToDownsample = 0;
  }

  ds_camera_msgs::core_image_t pkt;
  ds_camera_msgs::core_image_metadata_t metadata;
  pkt.utime = img->utime;
  pkt.width = img->width;
  pkt.height = img->height;
  pkt.row_stride = img->width; // fixed because we convert to 8-bit
  pkt.data = *(reinterpret_cast<uint8_t*>(img->imageData));
  pkt.size = img->width * img->height;
  pkt.nmetadata = 0;
  metadata.n = 0;
  metadata.value;

  pkt.metadatas.push_back(metadata);

  switch (img->imageFormat) {
    
    // Mono (B&W)
    case VmbPixelFormatMono16:
      convertImageTo8bit(pkt, img);
      // FALLTHROUGH
    case VmbPixelFormatMono8:
      pkt.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY;
      break;

    // RGB (color)
    case VmbPixelFormatRgb16:
      convertImageTo8bit(pkt, img);
      // FALLTHROUGH
    case VmbPixelFormatRgb8:
      pkt.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB;
      pkt.row_stride *= 3;
      pkt.size = img->width * img->height * 3;
      break;

    // everything else
    default:
      std::cerr<<"Unknown converted image format type!" <<std::endl;
      return;
  }

  if (useJpeg) {
    //std::cout <<"Using JPEG compression..." <<std::endl;
    jpegCompress(pkt, img);
  }

  //std::cout <<"Use JPEG: " <<useJpeg <<"\n";
  //std::cout <<"Raw Size: " <<pkt.width <<" x " <<pkt.height <<"\n";
  //std::cout <<"Stride: " <<pkt.row_stride <<" size: " <<pkt.size <<"\n";
  //image_pub_.publish(pkt);
}

void OutputLcmImage::convertImageTo8bit(ds_camera_msgs::core_image_t& pkt,
        const CamAcq::Image::ConstPtr& img) {

  // first, we need space
  int imgBytes = img->height * img->width;
  if (img->imageFormat == VmbPixelFormatRgb16) {
    imgBytes *= 3;
  }

  if (!imgBuf || imgBufSize < imgBytes) {
    imgBufSize = imgBytes;
    if (imgBuf) {
      free(imgBuf);
    }
    imgBuf = (uint8_t*)calloc(1, imgBufSize);
  }

  uint16_t* srcPtr = reinterpret_cast<uint16_t*>(img->image.Data);
  uint16_t* srcEndPtr = srcPtr + imgBytes; // # of bytes = # of pixels for 8bit
  for (uint8_t* destPtr = imgBuf; srcPtr < srcEndPtr; destPtr++, srcPtr++) {
    // take the most significant byte
    *destPtr = (uint8_t)((*srcPtr) >> 8);
  }

  pkt.size = imgBytes;
  pkt.data = *imgBuf;
}

// ONLY run AFTER converting to 8bit image
void OutputLcmImage::jpegCompress(ds_camera_msgs::core_image_t& pkt,
        const CamAcq::Image::ConstPtr& img) {

  // setup compression
  cinfo.err = jpeg_std_error( &jerr );
  jpeg_create_compress( &cinfo );
  jpeg_mem_dest( &cinfo, &jpegBuf, &jpegBufLen );

  cinfo.image_width = img->width;
  cinfo.image_height= img->height;
  switch(pkt.pixelformat) {
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY:
      cinfo.input_components = 1;
      cinfo.in_color_space = JCS_GRAYSCALE;
      break;

    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB:
      cinfo.input_components = 3;
      cinfo.in_color_space = JCS_RGB;
      break;
  }

  jpeg_set_defaults( &cinfo );
  if (jpegQuality > 0) {
    jpeg_set_quality ( &cinfo, jpegQuality, true );
  }
  jpeg_start_compress( &cinfo, true );
  JSAMPROW row_pointer;

  unsigned char* pkt_data = &pkt.data;
  // actually write the image
  while (cinfo.next_scanline < cinfo.image_height) {
    row_pointer = (JSAMPROW) & pkt_data[cinfo.next_scanline * pkt.row_stride];
    jpeg_write_scanlines( &cinfo, &row_pointer, 1 );
  }
  jpeg_finish_compress( &cinfo );

  // update the image we're sending out
  pkt.data = *jpegBuf;
  pkt.size = jpegBufLen;
  pkt.pixelformat = BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG;
}
