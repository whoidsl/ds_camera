#pragma once

#include <map>
#include <string>
#include <sstream>
#include <memory>
#include <map>
#include <boost/noncopyable.hpp>
#include <VimbaCPP/Include/VimbaCPP.h>
#include <VimbaImageTransform/Include/VmbTransform.h>
#include "ds_camera_msgs/attribute_t.h"

namespace CamAcq {

class Feature {
  public:
    typedef std::shared_ptr<Feature> Ptr;
    typedef std::shared_ptr<const Feature> ConstPtr;

    Feature(const std::string& _name) : name(_name) {};
    virtual ~Feature() = default;

    virtual std::string toString() const = 0;
    const std::string& getName() const { return name; };

    virtual int asInt() const = 0;
    virtual double asDouble() const = 0;

    template <typename T>
    T as() const; // we'll use explicit specializations in the .cpp

  protected:
    std::string name;
};

template <typename T>
class FeatureT : public Feature {
  public:
    typedef std::shared_ptr<FeatureT<T> > Ptr;
    typedef std::shared_ptr<const FeatureT<T> > ConstPtr;

    FeatureT(const std::string& _name) : Feature(_name) {};
    FeatureT(const std::string& _name, T _value) : Feature(_name), value(_value) {};
    virtual ~FeatureT() = default;
    
    virtual std::string toString() const {
      std::stringstream ret;
      ret <<value;
      return ret.str();
    }

    const T& getValue() const { return value; };
    
    template <typename R>
    R as() const {
      return static_cast<R>(value);
    }

    virtual int asInt() const {
      return static_cast<int>(value);
    }

    virtual double asDouble() const {
      return static_cast<double>(value);
    }

  protected:
    T value;
};

// there are explicit template specializations that prevent
// converting strings to numbers

typedef FeatureT<VmbBool_t>   FeatureBool;
typedef FeatureT<std::string> FeatureEnum;
typedef FeatureT<double>      FeatureFloat;
typedef FeatureT<VmbInt64_t>  FeatureInt;
typedef FeatureT<std::string> FeatureString;

Feature::ConstPtr FeatureFromDriver(const AVT::VmbAPI::FeaturePtr& f);

class Image : private boost::noncopyable {
  public:
    typedef std::shared_ptr<Image> Ptr;
    typedef std::shared_ptr<const Image> ConstPtr;

    Image(size_t payloadSize);
    virtual ~Image();

    void resetStats();

    AVT::VmbAPI::FramePtr raw_frame;
    std::map<std::string, Feature::ConstPtr> features;

    int64_t utime;
    VmbFrameStatusType status;
    VmbPixelFormatType rawFormat, imageFormat;
    VmbUint32_t ancillarySize, bufferSize;
    VmbUint32_t height, width;
    VmbUint32_t offsetX, offsetY;
    VmbUint64_t frameId, timestamp;
    bool forceNewFile; // the acquisition / processing demands a new file!

    std::string surveyID;
    std::vector<ds_camera_msgs::attribute_t> new_attrs;

    // semantics here are busted, so we'll keep our own pointer, thank you
    void* imageData;
    size_t imageDataSize; // size of buffer pointed to by imageData
    size_t imageBytes; // size of image that's actually in use

    VmbImage image; // points to imageData

    typedef uint8_t HistEntryT;
    std::vector<HistEntryT> histogram;

    // an intermediate buffer to use during image processing
    // Keep a pointer here to avoid constantly re-allocating
    void* intermedData;
    size_t intermedDataSize; // size of buffer pointed to by imageData

    void ensureImageDataSize(size_t bytes);
    void ensureIntermedDataSize(size_t bytes);
    
}; // Image class

}; // namespace
