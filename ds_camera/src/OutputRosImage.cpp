#include "OutputRosImage.h"

#include <iostream>
#include <iomanip>

#include "CameraUtil.h"


using namespace CamAcq;

OutputRosImage::OutputRosImage(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue& outputConfig) : config(driverConfig), camera(driverConfig), img_transport_(nh), AbstractOutput(driverConfig, outputConfig) {
    std::string subchannel;
    for (auto elem : outputConfig) {
        if (elem.first == "image_pub_topic") {
            subchannel = static_cast<std::string>(elem.second);
        }
    }
    imgChannel = driverConfig->getPublishPath()
                 + "/" + subchannel;
    image_pub_ = img_transport_.advertise(imgChannel, 1);
}

OutputRosImage::~OutputRosImage() {}

void OutputRosImage::handleImage(const CamAcq::Image::ConstPtr& img) {
    nh.getParam(nodeName+"/"+"ros_img_publish", publishToRos);
    if (!publishToRos) {
        ROS_WARN_STREAM_ONCE("ROS PUBLISHING TURNED OFF");
        return;
    } else {
        sensor_msgs::Image image;
        //VmbPixelFormatType pixel_format;
        std::string encoding;
        sensor_msgs::Image image_msg;

        //Take in processed image format (already debayered) and convert to proper ROS encoding
        switch (img->imageFormat) {
            case VmbPixelFormatMono8:
                encoding = sensor_msgs::image_encodings::MONO8;
                //ROS_WARN_STREAM("mono8");
                ROS_ERROR_STREAM_ONCE("please select a color output format for ros image publishing!");
                return;
                break;
            case VmbPixelFormatMono16:
                encoding = sensor_msgs::image_encodings::MONO16;
                //ROS_WARN_STREAM("mono16");
                ROS_ERROR_STREAM_ONCE("please select a color output format for ros image publishing!");
                return;
                break;
            case VmbPixelFormatRgb8:
                encoding = sensor_msgs::image_encodings::RGB8;
                //ROS_WARN_STREAM("rgb8");

                break;
            case VmbPixelFormatRgb16:
                encoding = sensor_msgs::image_encodings::TYPE_16UC3;
                //ROS_WARN_STREAM("rgb16");
                break;
            default:
                ROS_ERROR_STREAM("unknown converted image format");
        }
        bool res = false;
        const void *buffer_ptr = reinterpret_cast<uint8_t *>(img->imageData);
        int step = img->width * img->height * 3 / img->height;
        //res = sensor_msgs::fillImage(image_msg, encoding, img->height, img->width, step, buffer_ptr);
        image_msg.encoding = encoding;
        //ROS_ERROR_STREAM("encoding is: "<<image_msg.encoding);
        image_msg.height = img->height;
        image_msg.width = img->width;
        image_msg.step = img->width * img->height * 3 / img->height;
        size_t st0 = image_msg.step * img->height;
        image_msg.data.resize(st0);
        //ROS_ERROR_STREAM("img datasize: "<<st0);
        memcpy(&image_msg.data[0], buffer_ptr, st0);
        image_msg.is_bigendian = 0;
        image_msg.header.stamp = static_cast<ros::Time>(img->utime * .0000001);
        image_msg.header.frame_id = config->surveyID;
        image_pub_.publish(image_msg);
        /*
        if (!res) {
            ROS_ERROR_STREAM("COULD NOT FILL ROS IMAGE");
        } else {
            image_msg.header.stamp = static_cast<ros::Time>(img->utime * .00000001);
            image_msg.header.frame_id = config->surveyID;
            image_pub_.publish(image_msg);
        }
         */
    }
}

