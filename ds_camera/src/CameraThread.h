#pragma once

#include "AVTCamera.h"
#include "DriverThread.h"
#include "DriverQueue.h"
#include "DriverImage.h"

#include <vector>
#include <mutex>

//#include <herc_acq/attribute_t.hpp>
//#include "sentry_cam/herc_acq_attribute_t.h"

namespace CamAcq {
class CamThread : public CamAcq::Thread {
  public:
    typedef std::shared_ptr<CamThread> Ptr;
    typedef std::shared_ptr<const CamThread> ConstPtr;

    CamThread(const CameraConfig::Ptr& cfg);
    virtual ~CamThread() = default;

    virtual void threadMain();
    virtual void fillInStatus(ds_camera_msgs::camera_t &state,
            std::vector<ds_camera_msgs::status_t> &statuses);

    virtual void fillInShutdownStatus(ds_camera_msgs::camera_t &state,
            std::vector<ds_camera_msgs::status_t> &statuses);

    void startAcq();
    void stopAcq();
    void setSurveyID(const std::string& _sid) { surveyID = _sid; };
    const std::string& getSurveyID() const {return surveyID; };
    void setAttr(const ds_camera_msgs::attribute_t& attr);

    typedef CamAcq::DriverQueue<CamAcq::Image::Ptr> OutputQueue_t;
    const OutputQueue_t::Ptr& getOutput() const { return output; };

    enum { STATE_RUNNING = 0 };
    enum { STATE_READY = 1 };
    enum { STATE_WAITING_FOR_CAMERA = 2 };
    enum { STATE_WAITING_FOR_SURVEY = 3 };
    enum { STATE_QUIT = 4 };


protected:
    CameraConfig::Ptr config;
    AVTCamera camera;
    std::string surveyID;


    OutputQueue_t::Ptr output;
    void frameCallback(const CamAcq::Image::Ptr& img);

    std::mutex attr_mutex;
    std::vector<ds_camera_msgs::attribute_t> new_attrs;

    float statusTtlSeconds;
    double statusImportance;



};
};
