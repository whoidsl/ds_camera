#include "DriverThread.h"

#include <iostream>

CamAcq::Thread::Thread() {
  running.test_and_set(); // mark as ready
}
CamAcq::Thread::~Thread() {
  if (running.test_and_set()) {
    stop();
  }
}

static void runThreadMain(CamAcq::Thread* obj) {
  obj->threadMain();
  obj->stop_nonblock(); // clears the running flag
}

/// \brief Starts the thread
void CamAcq::Thread::start(){
  // DO NOT mark as running-- this could have been changed 
  // since we were initialized!
  inner_thread = std::thread(runThreadMain, this);
}

/// \brief Stops the thread. Called from outside
void CamAcq::Thread::stop() {
  running.clear();
  if (inner_thread.joinable()) {
    inner_thread.join();
  }
}

void CamAcq::Thread::stop_nonblock() {
  running.clear();
}
