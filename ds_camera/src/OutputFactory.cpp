#include "OutputFactory.h"

#include "OutputConsole.h"
#include "OutputLcmImage.h"
#include "OutputTiff.h"
#include "OutputSdl.h"
#include "OutputRosImage.h"

#include <stdexcept>

using namespace CamAcq;

OutputFactory::OutputFactory() {
  factories[OUTNAME_CONSOLE] = boost::factory<OutputConsole*>();
  factories[OUTNAME_LCMIMG] = boost::factory<OutputLcmImage*>();
  factories[OUTNAME_ROSIMG] = boost::factory<OutputRosImage*>();
  factories[OUTNAME_TIFF] = boost::factory<OutputTiff*>();
  factories[OUTNAME_SDL] = boost::factory<OutputSdl*>();
}

void OutputFactory::registerClass(const std::string& type, const Factory& newCons) {
  factories[type] = newCons;
}

CamAcq::AbstractOutput::Ptr OutputFactory::makeObj(const CameraConfig::Ptr& driverConfig, XmlRpc::XmlRpcValue outputConfig) {
        CamAcq::AbstractOutput::Ptr ret = nullptr;
        std::string output_type;
        for (auto elem : outputConfig) {
            if (elem.first == "type") {
                output_type = static_cast<std::string>(elem.second);
                ROS_INFO_STREAM("output type: "<<output_type);
            }
        }
        std::string type = output_type;
        auto iter = factories.find(type);
        if (iter == factories.end()) {
            ROS_ERROR_STREAM("UNKNOWN OUTPUT TYPE: "<<type);
        }
        return CamAcq::AbstractOutput::Ptr((*iter).second(driverConfig, outputConfig));
    }

