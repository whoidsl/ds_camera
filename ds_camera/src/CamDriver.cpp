#include "CamDriver.h"

#include <unistd.h>
#include <iostream>
#include <thread>

#include "AbstractOutput.h"

#include "OutputFactory.h"

CamAcq::CamDriver::CamDriver(const CameraConfig::Ptr& c) : config(c) {
  running.clear();

  // start with the standard threads
  camThread.reset(new CamThread(config));
  procThread.reset(new ProcessThread(config));
  procThread->setInput(camThread->getOutput());

  attrThread.reset(new AttributeThread(config));
  attrThread->setAcqThread(camThread);
  attrThread->addThread(camThread);
  attrThread->addThread(procThread);

  // now do the outputs
  CamAcq::OutputFactory factory;
  for (size_t i=0; i<config->getOutputs().size(); i++) {
    XmlRpc::XmlRpcValue out = config->getOutputs()[i];
    //ROS_WARN_STREAM("out: "<<out);

    bool diskLog = config->logToDisk;
    if (!diskLog) {
        ROS_WARN_STREAM("not logging to disk");
        continue;
    }
    outputs.push_back(factory.makeObj(config, out));
    procThread->addOutput(outputs.back()->getInput());
    attrThread->addThread(outputs.back());
    if (outputs.back()->isDisplayOutput()) {
      if (displayThread.get() != NULL) {
        throw std::runtime_error("Attempted to use more than one display output!");
      }
      displayThread = outputs.back();
    }
  }

  running.test_and_set(); // flag now, could get reset between
                          // now and start
}

CamAcq::CamDriver::~CamDriver() {
  if (running.test_and_set()) {
    stop();
  }
}

void CamAcq::CamDriver::start() {
  // start outputs first
  for (std::vector<CamAcq::AbstractOutput::Ptr>::const_iterator iter = outputs.begin();
      iter != outputs.end(); iter++) {
    (*iter)->start();
  }

  procThread->start();
  camThread->start();
  attrThread->start(); // start last so everything is ready for user input

  while (running.test_and_set()) {
    if (displayThread.get() != NULL) {
      displayThread->threadMain();
    } else {
      // no GUI thread, just sleep
      usleep(200000);
    }
  }
  stop();
}

void CamAcq::CamDriver::stop() {
  // stop the outputs first
  for (std::vector<CamAcq::AbstractOutput::Ptr>::const_iterator iter = outputs.begin();
      iter != outputs.end(); iter++) {
    (*iter)->stop_nonblock();
  }
  
  // stop accepting commands
  attrThread->stop();

  // stop the inputs
  camThread->stop();
  procThread->stop();

  // wait for all the outputs
  for (std::vector<CamAcq::AbstractOutput::Ptr>::const_iterator iter = outputs.begin();
      iter != outputs.end(); iter++) {
    (*iter)->stop();
  }

  running.clear();
}

void CamAcq::CamDriver::stop_nonblock() {
  running.clear();
  attrThread->stop_nonblock();
  camThread->stop_nonblock();
  procThread->stop_nonblock();
  for (std::vector<CamAcq::AbstractOutput::Ptr>::const_iterator iter = outputs.begin();
      iter != outputs.end(); iter++) {
    (*iter)->stop_nonblock();
  }
}

bool CamAcq::CamDriver::isRunning() {
  // this is mostly thread-safe.
  // It's possible another thread could hit this inside the 
  // if (running.test_and_set) statement and falsely decide we're 
  // still running.  But everything has to keep polling that
  // periodically, so it's OK.  TL;DR: it might make things a little
  // slower, but that's pretty unlikley
  if (! running.test_and_set() ) {
    // NOT running!
    running.clear(); // restore to zero
    return false;
  }
  return true;
}
