#include "OutputTiff.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <unistd.h>
//#include <tif_dir.h> // old?
#include <boost/date_time/posix_time/posix_time.hpp>


#include "CameraUtil.h"
#include "prosilicaDrvTags.h"

#define STATUS_OK 0
#define STATUS_ALERT 1
#define STATUS_WARN 2
#define STATUS_ERROR 3

using namespace CamAcq;


template<typename T>
void writeFeatureTifftag_multi(TIFF* outfile, const CamAcq::Image::ConstPtr& img, int TAGID, const std::vector<std::string>& featureNames, bool complain=true) {

  T value;
  for (size_t i=0; i<featureNames.size(); i++) {
    auto iter = img->features.find(featureNames[i]);
    //ROS_ERROR_STREAM("feature: "<<featureNames[i]);
    if (iter == img->features.end()) {
      continue;
    }
    value = (iter->second)->as<T>();
    TIFFSetField(outfile, TAGID, value);
    //std::cout <<"Using field: " <<featureNames[i] <<"\n";
    return;
  }
  if (complain) {
    //std::cerr <<"Unable to find feature " <<featureNames[0] <<"\n";
    ROS_ERROR_STREAM_ONCE("Unable to find feature "<<featureNames[0]);
  }
}

// strings are PITA, jsut specialize and move on
template<>
void writeFeatureTifftag_multi<std::string>(TIFF* outfile, const CamAcq::Image::ConstPtr& img, int TAGID, const std::vector<std::string>& featureNames, bool complain) {

  std::string value;
  for (size_t i=0; i<featureNames.size(); i++) {
    auto iter = img->features.find(featureNames[i]);
    //ROS_ERROR_STREAM("feature: "<<featureNames[i]);
    if (iter == img->features.end()) {
      continue;
    }
    value = (iter->second)->toString();
    TIFFSetField(outfile, TAGID, value.c_str());
    //std::cout <<"Using field: " <<featureNames[i] <<"\n";
    return;
  }
  if (complain) {
    //std::cerr <<"Unable to find feature " <<featureNames[0] <<"\n";
    ROS_ERROR_STREAM_ONCE("Unable to find feature "<<featureNames[0]);

  }
}

template<typename T>
void writeFeatureTifftag(TIFF* outfile, const CamAcq::Image::ConstPtr& img, int TAGID, const std::string& featureName, bool complain=true) {
  std::vector<std::string> args = {featureName};
  writeFeatureTifftag_multi<T>(outfile, img, TAGID, args, complain);
};


OutputTiff::OutputTiff(const CameraConfig::Ptr& driverConfig, 
    XmlRpc::XmlRpcValue& outputConfig)
  : AbstractOutput(driverConfig, outputConfig) {

  ros::NodeHandle nh;
  ros::Publisher tiff_info_pub_ = nh.advertise<ds_camera_msgs::image_info_t>("tiff_info", 1000);

  for (auto elem : outputConfig) {
      if (elem.first == "name") {
          name = static_cast<std::string>(elem.second);
      }
      else if (elem.first == "outputDir") {
          root = static_cast<std::string>(elem.second);
          ROS_WARN_STREAM("output directory is: "<< root);
      }
      else if (elem.first == "spaceStatus") {
          spaceStatus = static_cast<int>(elem.second);
      }
  }
  camName = driverConfig->getName();

  // parse some options, with sane default choices
  statusImportance = spaceStatus + driverConfig->cameraStatusImportance;

  statusTtlSeconds = driverConfig->updateInterval * 5.0;

  freeMbytesWarn = 100;
  freeMbytesError = 25;
  outfile = NULL;
  outfileImages = 0;
  imagesPerFile = 1;
  compression = COMPRESSION_NONE;
  std::string compress_str;
  for (auto elem : outputConfig) {
      if (elem.first == "warnMbytes") {
          freeMbytesWarn = static_cast<int>(elem.second);
      } else if (elem.first == "errorMbytes") {
          freeMbytesError = static_cast<int>(elem.second);
      } else if (elem.first == "imagesPerFile") {
          imagesPerFile = static_cast<int>(elem.second);
      } else if (elem.first == "compression") {
          compress_str = static_cast<std::string>(elem.second);
          if (compress_str == "lzw") {
              compression = COMPRESSION_LZW;
          } else if (compress_str == "deflate") {
              compression = COMPRESSION_DEFLATE;
          } else if (compress_str != "none") {
              ROS_WARN_STREAM("TIFF Compression mode \"" << compress_str << "\" unsupported, aborting compression!\n");
          }
      } else if (elem.first == "artist") {
          copyright = static_cast<std::string>(elem.second);
      } else if (elem.first == "copyright") {
          copyright = static_cast<std::string>(elem.second);
      }
  }

  const size_t HOSTNAME_LEN = 255;
  char hostnamebuf[HOSTNAME_LEN];
  int rc = gethostname(hostnamebuf, HOSTNAME_LEN);
  if (rc) {
    std::cerr <<"Unable to get hostname!\n";
    hostcomputer = "UNKNOWN";
  } else {
    hostnamebuf[HOSTNAME_LEN-1] = '\0'; // make SURE its NULL-terminated
    hostcomputer = std::string(hostnamebuf);
  }
}

OutputTiff::~OutputTiff() {
  if(outfile) {
    TIFFClose(outfile);
    outfile = NULL;
    outfileImages = 0;
  }
}

void OutputTiff::handleImage(const CamAcq::Image::ConstPtr& img) {
    bool diskLog;
    nh.getParam(nodeName+"/"+"log_to_disk", diskLog);
    if (!diskLog) {
        ROS_WARN_STREAM_ONCE("Log to Disk is: "<<diskLog<<" !");
        return;
    }
  // prepare the output directory
  if (currentSurveyId != img->surveyID) {
      ROS_WARN_STREAM("Setting new survey ID");
    currentSurveyId = img->surveyID;

    if (outfile) {
      // close the existing file
      TIFFClose(outfile);
      outfile = NULL;
      outfileImages = 0;
    }
    outputDir = root;
    outputDir /= currentSurveyId;
    outputDir /= "raw";

    if (!boost::filesystem::exists(outputDir)) {
      boost::filesystem::create_directories(outputDir);
    }
  }
  if (!boost::filesystem::is_directory(outputDir)) {
    std::cerr <<"Attempting to write image to \"" <<outputDir <<"\" which is not a directory\n";
  }

  // ok, we're ready to start writing an image
  boost::filesystem::path outputFile(outputDir);
  std::string datetimeStr;
  nh.getParam(nodeName + "/" + "system", systemName);
  std::string filename = makeFilename(systemName, datetimeStr, img->utime, img->frameId);
  //std::string filename = makeFilename(img->utime, img->frameId, datetimeStr);
  outputFile /= filename;

  //std::cout <<"Writing output file to \"" <<outputFile.string() <<"\"\n";

  // check to see if we've been asked to start a new file
  if (img->forceNewFile) {
    std::cout <<"TIFFOUT: Driver demands new file!\n";
    if (outfile) {
      TIFFClose(outfile);
      outfile = NULL;
      outfileImages = 0;
    }
  }

  // if necessary, open the output file
  if (!outfile) {
    outfile = TIFFOpen(outputFile.string().c_str(), "w");
    if (!outfile) {
      std::cout <<"TIFFOpen() FAILED! Filename: " <<outputFile.string() <<"\n";
      return;
    }
    outfileImages = 0;
  }

  // define some new tifftags:
  TIFFMergeFieldInfo(outfile, ProTiffFieldInfo, PROTIFFTAG_N);
  // this is the old way of doing this?
  //const TIFFFieldArray *exif_fields = _TIFFGetExifFields(); 
  //_TIFFMergeFields(outfile, exif_fields->fields, exif_fields->count); 


  // fill in the metadata
  // start with our custom fields
  int32_t utime_lo = static_cast<int32_t>( img->utime & 0xffffffff );
  int32_t utime_hi = static_cast<int32_t>( static_cast<uint64_t>(img->utime) >> 32);
  int32_t img_num  = static_cast<int32_t>( img->frameId & 0xffffffff );
  TIFFSetField(outfile, PROTIFFTAG_MAGIC, (uint32_t)PROTIFFTAG_MAGIC_VALUE);
  TIFFSetField(outfile, PROTIFFTAG_VERSION, PROTIFFTAG_CURRENT_VERSION);
  writeFeatureTifftag<int32_t>    (outfile, img,       PROTIFFTAG_BINNINGHORIZONTAL, "BinningHorizontal");
  writeFeatureTifftag<int32_t>    (outfile, img,         PROTIFFTAG_BINNINGVERTICAL, "BinningVertical");
  writeFeatureTifftag<int32_t>    (outfile, img,      PROTIFFTAG_EXPOSUREAUTOTARGET, "ExposureAutoTarget");
  writeFeatureTifftag<std::string>(outfile, img,            PROTIFFTAG_EXPOSUREAUTO, "ExposureAuto");
  std::vector<std::string> exposureNames = {"ChunkExposureTime", "ExposureTimeAbs"};
  writeFeatureTifftag_multi<int32_t>(outfile, img,         PROTIFFTAG_EXPOSURETIMEABS, exposureNames);
  writeFeatureTifftag<double>     (outfile, img, PROTIFFTAG_ACQUISITIONFRAMERATEABS, "AcquisitionFrameRateAbs");
  std::vector<std::string> gainNames = {"ChunkGain", "GainRaw", "Gain"};
  writeFeatureTifftag_multi<int32_t>(outfile, img,                    PROTIFFTAG_GAIN, gainNames);
  writeFeatureTifftag<int32_t>      (outfile, img,                  PROTIFFTAG_HEIGHT, "Height");
  writeFeatureTifftag<int32_t>      (outfile, img,                 PROTIFFTAG_OFFSETX, "OffsetX");
  writeFeatureTifftag<int32_t>      (outfile, img,                 PROTIFFTAG_OFFSETY, "OffsetY");
  writeFeatureTifftag<double>(outfile, img,           PROTIFFTAG_STATFRAMERATE, "StatFrameRate");
  writeFeatureTifftag<int32_t>      (outfile, img,      PROTIFFTAG_STATFRAMEDELIVERED, "StatFrameDelivered");
  writeFeatureTifftag<int32_t>      (outfile, img,        PROTIFFTAG_STATFRAMEDROPPED, "StatFrameDropped");
  writeFeatureTifftag<int32_t>      (outfile, img,    PROTIFFTAG_STREAMBYTESPERSECOND, "StreamBytesPerSecond");
  writeFeatureTifftag<int32_t>      (outfile, img,                   PROTIFFTAG_WIDTH, "Width");
  writeFeatureTifftag<std::string>(outfile, img,                PROTIFFTAG_NIR_MODE, "NirMode", false);
  TIFFSetField(outfile, PROTIFFTAG_UTIME_LO, utime_lo);
  TIFFSetField(outfile, PROTIFFTAG_UTIME_HI, utime_hi);
  TIFFSetField(outfile, PROTIFFTAG_IMG_NUM, img_num);
  TIFFSetField(outfile, PROTIFFTAG_IMG_FILENAME, filename.c_str());
  TIFFSetField(outfile, PROTIFFTAG_IMG_SURVEYID, currentSurveyId.c_str());

  // set some standard metadata tifftags
  TIFFSetField(outfile, TIFFTAG_DOCUMENTNAME, filename.c_str());
  writeFeatureTifftag<std::string>(outfile, img, TIFFTAG_MAKE, "DeviceVendorName");
  writeFeatureTifftag<std::string>(outfile, img, TIFFTAG_MODEL, "DeviceModelName");
  writeFeatureTifftag<std::string>(outfile, img, TIFFTAG_CAMERASERIALNUMBER, "DeviceID");
  writeFeatureTifftag<std::string>(outfile, img, TIFFTAG_SOFTWARE, "DeviceFirmwareVersion");
  if (!artist.empty()) {
    TIFFSetField(outfile, TIFFTAG_ARTIST, artist.c_str());
  }
  if (!copyright.empty()) {
    TIFFSetField(outfile, TIFFTAG_COPYRIGHT, copyright.c_str());
  }


  TIFFSetField(outfile, TIFFTAG_HOSTCOMPUTER, hostcomputer.c_str());
  TIFFSetField(outfile, TIFFTAG_DATETIME, datetimeStr.c_str());
  //TIFFSetField(outfile, EXIFTAG_DATETIMEORIGINAL, datetimeStr.c_str()); // Unknown??

  // position of image in frame
  writeFeatureTifftag<int32_t>     (outfile, img, TIFFTAG_PIXAR_IMAGEFULLWIDTH, "WidthMax");
  writeFeatureTifftag<int32_t>     (outfile, img, TIFFTAG_PIXAR_IMAGEFULLLENGTH, "HeightMax");
  writeFeatureTifftag<int32_t>     (outfile, img, TIFFTAG_XPOSITION, "OffsetX");
  writeFeatureTifftag<int32_t>     (outfile, img, TIFFTAG_YPOSITION, "OffsetY");

  // imaging metadata stuff (TODO)
  //EXIFTAG_FOCALLENGTH
  //EXIFTAG_FOCALLENGTHIN35MMFILM
  //EXIFTAG_EXPOSURETIME
  //EXIFTAG_FNUMBER	
  //EXIFTAG_EXPOSUREPROGRAM

  // camera calibration (TODO)
  //TIFFTAG_CAMERACALIBRATION1
  //TIFFTAG_CAMERACALIBRATION2
  //TIFFTAG_CALIBRATIONILLUMINANT1
  //TIFFTAG_CALIBRATIONILLUMINANT2

  
  // fill in the image data / formatting stuff / etc
  size_t stride=0;
  bool sixteen_bit = false;
  switch (img->imageFormat) {
    
    // Mono (B&W)

    case VmbPixelFormatMono8:
      TIFFSetField (outfile, TIFFTAG_SAMPLESPERPIXEL, 1);
      TIFFSetField (outfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
      TIFFSetField (outfile, TIFFTAG_BITSPERSAMPLE, 8);
      sixteen_bit = false;
      stride = img->width;
      break;

    case VmbPixelFormatMono16:
      TIFFSetField (outfile, TIFFTAG_SAMPLESPERPIXEL, 1);
      TIFFSetField (outfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
      TIFFSetField (outfile, TIFFTAG_BITSPERSAMPLE, 16);
      sixteen_bit = true;
      stride = 2*img->width;
      break;

    // RGB (color)
    case VmbPixelFormatRgb8:
      TIFFSetField (outfile, TIFFTAG_SAMPLESPERPIXEL, 3);
      TIFFSetField (outfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
      TIFFSetField (outfile, TIFFTAG_BITSPERSAMPLE, 8);
      stride = 3*img->width;
      sixteen_bit = false;
      break;

    case VmbPixelFormatRgb16:
      TIFFSetField (outfile, TIFFTAG_SAMPLESPERPIXEL, 3);
      TIFFSetField (outfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
      TIFFSetField (outfile, TIFFTAG_BITSPERSAMPLE, 16);
      stride = 6*img->width;
      sixteen_bit = true;
      break;

    // everything else
    default:
      std::cerr<<"Unknown converted image format type!" <<std::endl;
      return;
  }
  TIFFSetField (outfile, TIFFTAG_IMAGEWIDTH, img->width);
  TIFFSetField (outfile, TIFFTAG_IMAGELENGTH, img->height);
  TIFFSetField (outfile, TIFFTAG_ROWSPERSTRIP, 1);
  TIFFSetField (outfile, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
  TIFFSetField (outfile, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);

  // setup compression (we only allow lossless!, you should use "lzw" or "none")
  TIFFSetField (outfile, TIFFTAG_COMPRESSION, compression);

  // actually copy the pixels
  int rc;
  uint8_t* row_ptr = reinterpret_cast<uint8_t*>(img->image.Data);
  for (size_t i=0; i<img->height; i++) {
    rc = TIFFWriteScanline(outfile, row_ptr, i, 0);
    row_ptr += stride;
    if (rc < 0) {
      break;
    }
  }
  if (rc < 0) {
    std::cerr <<"Unable to write TIFF!" <<std::endl;
  }

  // finish up / close the image / whatever
  outfileImages++;
  if (outfileImages >= imagesPerFile) {
    TIFFClose(outfile);
    outfile = NULL;
    outfileImages = 0;
  } else {
    int rc = TIFFWriteDirectory(outfile);
    if (rc != 1) {
      std::cerr <<"Error writing TIFF directory!\n";
    }
  }
  ds_camera_msgs::image_info_t imageInfo;
  imageInfo.path = outputFile.string();
  //tiff_info_pub_.publish(imageInfo);

}

void OutputTiff::fillInStatus(ds_camera_msgs::camera_t& state,
        std::vector<ds_camera_msgs::status_t>& statuses) {

  size_t freeMbytes;
  if (currentSurveyId.empty()) {
    freeMbytes = 0;
  } else {
    boost::filesystem::space_info space;
    space = boost::filesystem::space(outputDir);
    freeMbytes = space.available / 1048576; // 1024*1024
  }



  ds_camera_msgs::attribute_t spaceAttr;
  spaceAttr.object = name;
  spaceAttr.label = "FreeMB";
  spaceAttr.value = freeMbytes;
  state.attributes.push_back(spaceAttr);

  if (!state.outputPath.empty()) {
    state.outputPath += "\n";
    state.outputPath += outputDir.string();
  } else {
    state.outputPath = outputDir.string();
  }

  ds_camera_msgs::status_t status;
  status.utime = getUtimeNow();
  status.key = camName + "_diskSpace";
  if (name.length() > 5) {
    status.label = camName.substr(0, 5);
  } else {
    status.label = camName;
  }
  status.label += " HD";

  std::stringstream disp;
  disp.precision(3);
  if (freeMbytes > (1024*1024*1024)) {
    disp <<static_cast<double>(freeMbytes)/(1024.0*1024.0*1024.0) <<" PB";

  } else if (freeMbytes > (1024*1024)) {
    disp <<static_cast<double>(freeMbytes)/(1024.0*1024.0) <<" TB";

  } else if (freeMbytes > 1024) {
    disp <<static_cast<double>(freeMbytes)/1024.0 <<" GB";

  } else {
    disp <<freeMbytes <<" MB";
  }
  if (currentSurveyId.empty()) {
    status.display = "?";
    status.message = "No \"" + camName + "\" images written to disk yet!";
    status.status = STATUS_ALERT;
  } else {
    status.display = disp.str();

    status.message = "Disk for camera \"" + camName + "\" output \"" + name + "\"";
    if (freeMbytes < freeMbytesError) {
      status.status = STATUS_ERROR;
      status.message += " is almost full!";
    } else if (freeMbytes < freeMbytesWarn) {
      status.status = STATUS_WARN;
      status.message += " is filling up!";
    } else {
      status.status = STATUS_OK;
      status.message += " has free space";
    }
    status.message += "\nPATH: ";
    status.message += outputDir.string();
  } // if have valid output directory
  
  status.alertOnWarning = true;
  status.alertOnError   = true;
  status.errorOnExpire  = false;
  status.ttl_seconds = statusTtlSeconds;
  status.importance = statusImportance;
  
  statuses.push_back(status);
}


std::string OutputTiff::makeFilename(std::string& systemName, std::string& isodatetime, int64_t utime, int64_t frameNumber) const {

  time_t tt = utime / 1000000;
  int usec = utime % 1000000;

  boost::posix_time::ptime tv = boost::posix_time::from_time_t(tt);
  std::stringstream fname;
  //fname <<"IMG-" <<camName <<"-";
  fname <<systemName<<".";
  fname <<boost::gregorian::to_iso_string(tv.date()) <<".";
  fname <<boost::posix_time::to_iso_string(tv.time_of_day());
  std::ios_base::fmtflags flags = fname.flags();
  fname <<std::setw(6) <<std::setfill('0') <<usec <<".";
  fname.flags(flags);
  fname <<frameNumber <<".tif";

  std::stringstream datetimestr;
  datetimestr <<boost::posix_time::to_iso_extended_string(tv);
  datetimestr <<".";
  datetimestr <<std::setw(6) <<std::setfill('0') <<usec; 
  datetimestr <<"Z";

  isodatetime = datetimestr.str();

  return fname.str();

}

