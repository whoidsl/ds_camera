INCLUDE(FindPackageHandleStandardArgs)

SET(SDL_gfx_IncludeSearchPaths
	/usr/include/SDL
	/usr/local/include/SDL
	/opt/local/include/SDL
)

set(SDL_gfx_LibrarySearchPaths
	/usr/lib
	/usr/local/lib
	/opt/local/lib
)

FIND_PATH(SDL_gfx_INCLUDE_DIR SDL_gfxPrimitives.h
	PATHS ${SDL_gfx_IncludeSearchPaths})

FIND_LIBRARY(SDL_gfx_LIBRARY
	NAMES SDL_gfx
	PATHS ${SDL_gfx_LibrarySearchPaths})

#message(STATUS "SDL_gfx_LIBRARY: ${SDL_gfx_LIBRARY}")
#message(STATUS "SDL_gfx_INCLUDE: ${SDL_gfx_INCLUDE_DIR}")

FIND_PACKAGE_HANDLE_STANDARD_ARGS(SDL_gfx "Could not find SDL_gfx library"
	SDL_gfx_LIBRARY
	SDL_gfx_INCLUDE_DIR
)

MARK_AS_ADVANCED(
	SDL_gfx_INCLUDE_DIR
	SDL_gfx_LIBRARY)

