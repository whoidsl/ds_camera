INCLUDE(FindPackageHandleStandardArgs)

SET(Jpeg_IncludeSearchPaths
  /usr/include
  /usr/local/include
  /opt/local/include
)

set(Jpeg_LibrarySearchPaths
	/usr/lib
	/usr/local/lib
	/opt/local/lib
)

FIND_PATH(Jpeg_INCLUDE_DIR jpeglib.h
	PATHS ${Jpeg_IncludeSearchPaths})

FIND_LIBRARY(Jpeg_LIBRARY
	NAMES jpeg
	PATHS ${Jpeg_LibrarySearchPaths})

FIND_PACKAGE_HANDLE_STANDARD_ARGS(Jpeg "Could not find libjpeg library"
	Jpeg_LIBRARY
	Jpeg_INCLUDE_DIR
)

MARK_AS_ADVANCED(
	Jpeg_INCLUDE_DIR
	Jpeg_LIBRARY)

