INCLUDE(FindPackageHandleStandardArgs)

SET(VimbaPath "/usr/local/Vimba_3_1")
SET(VimbaSDK_IncludeSearchPaths
        ${VimbaPath}
	"/usr/local/"
        )

SET(VimbaSDK_LibrarySearchPaths
        ${VimbaPath}/VimbaC/DynamicLib/x86_64bit/
        ${VimbaPath}/VimbaCPP/DynamicLib/x86_64bit/
        ${VimbaPath}/VimbaImageTransform/DynamicLib/x86_64bit/
        )

FIND_PATH(VimbaSDK_INCLUDE_DIR VimbaC/Include/VmbCommonTypes.h
        PATHS ${VimbaSDK_IncludeSearchPaths}
        )
FIND_LIBRARY(VimbaSDK_C_LIBRARY
        NAMES VimbaC
        PATHS ${VimbaSDK_LibrarySearchPaths}
        )
FIND_LIBRARY(VimbaSDK_CPP_LIBRARY
        NAMES VimbaCPP
        PATHS ${VimbaSDK_LibrarySearchPaths}
        )
FIND_LIBRARY(VimbaSDK_TFORM_LIBRARY
        NAMES VimbaImageTransform
        PATHS ${VimbaSDK_LibrarySearchPaths}
        )
set(VimbaSDK_LIBRARIES ${VimbaSDK_C_LIBRARY} ${VimbaSDK_CPP_LIBRARY} ${VimbaSDK_TFORM_LIBRARY})

# Handle the REQUIRED argument and set the <UPPERCASED_NAME>_FOUND variable
FIND_PACKAGE_HANDLE_STANDARD_ARGS(VimbaSDK "Could NOT find VimbaSDK library (Prosilica / Allied Vision Tech cameras)"
        VimbaSDK_LIBRARIES
        VimbaSDK_INCLUDE_DIR
        )

MARK_AS_ADVANCED(
        VimbaSDK_INCLUDE_DIR
        VimbaSDK_LIBRARY
)
