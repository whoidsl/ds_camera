# Install script for directory: /home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/ivandor/catkin_ws/src/oet_cam/build")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/pod-build/lib/liblcmtypes_bot2-param.a")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes" TYPE FILE FILES
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/c/lcmtypes/bot_param_update_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/c/lcmtypes/bot_param_entry_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/c/lcmtypes/bot_param_set_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/c/lcmtypes/bot_param_request_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/c/lcmtypes/bot2_param.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/pod-build/lib/pkgconfig/lcmtypes_bot2-param.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/cpp/lcmtypes/bot_param/request_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/cpp/lcmtypes/bot_param/set_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/cpp/lcmtypes/bot_param/update_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/cpp/lcmtypes/bot_param/entry_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/cpp/lcmtypes/bot2_param.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/java" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/pod-build/lcmtypes_bot2-param.jar")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param/update_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/python/bot_param/update_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param/set_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/python/bot_param/set_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param/entry_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/python/bot_param/entry_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param/__init__.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/python/bot_param/__init__.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param/request_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_param" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/python/bot_param/request_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/lcmtypes" TYPE FILE FILES
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/bot_param_entry_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/bot_param_request_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/bot_param_update_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/lcmtypes/bot_param_set_t.lcm"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/pod-build/src/param_client/cmake_install.cmake")
  include("/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/pod-build/src/param_server/cmake_install.cmake")
  include("/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/pod-build/src/param_tester/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-param/pod-build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
