# Install script for directory: /home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/ivandor/catkin_ws/src/oet_cam/build")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/lib/liblcmtypes_bot2-core.a")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes" TYPE FILE FILES
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core_image_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core_raw_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core_pose_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core_image_sync_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core_sensor_status_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core_planar_lidar_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core_image_metadata_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core_rigid_transform_t.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/c/lcmtypes/bot_core.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/lib/pkgconfig/lcmtypes_bot2-core.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core/planar_lidar_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core/rigid_transform_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core/raw_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core/image_sync_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core/image_metadata_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core/pose_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core/sensor_status_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core/image_t.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/cpp/lcmtypes/bot_core.hpp")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/java" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/lcmtypes_bot2-core.jar")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/sensor_status_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/sensor_status_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/image_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/image_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/raw_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/raw_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/image_sync_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/image_sync_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/__init__.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/__init__.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/rigid_transform_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/rigid_transform_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/pose_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/pose_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/planar_lidar_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/planar_lidar_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core/image_metadata_t.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/ivandor/catkin_ws/src/oet_cam/build/lib/python2.7/dist-packages/bot_core" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/python/bot_core/image_metadata_t.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/lcmtypes" TYPE FILE FILES
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/bot_core_image_sync_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/bot_core_rigid_transform_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/bot_core_planar_lidar_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/bot_core_image_metadata_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/bot_core_raw_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/bot_core_sensor_status_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/bot_core_image_t.lcm"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/lcmtypes/bot_core_pose_t.lcm"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/src/bot_core/cmake_install.cmake")
  include("/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/java/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
