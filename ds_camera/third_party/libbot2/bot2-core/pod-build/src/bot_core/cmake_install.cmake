# Install script for directory: /home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/ivandor/catkin_ws/src/oet_cam/build")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libbot2-core.so.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libbot2-core.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "/home/ivandor/catkin_ws/src/oet_cam/build/lib")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/lib/libbot2-core.so.1"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/lib/libbot2-core.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libbot2-core.so.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libbot2-core.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/lib:/home/ivandor/catkin_ws/src/oet_cam/build/lib:"
           NEW_RPATH "/home/ivandor/catkin_ws/src/oet_cam/build/lib")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/bot_core" TYPE FILE FILES
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/ptr_circular.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/ppm.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/color_util.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/set.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/timespec.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/serial.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/ssocket.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/glib_util.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/rand_util.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/rotations.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/small_linalg.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/lcm_util.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/fasttrig.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/timestamp.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/circular.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/ringbuf.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/bot_core.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/ctrans.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/math_util.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/camtrans.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/trans.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/minheap.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/signal_pipe.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/fileutils.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/tictoc.h"
    "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/src/bot_core/gps_linearize.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/ivandor/catkin_ws/src/oet_cam/third_party/libbot2/bot2-core/pod-build/lib/pkgconfig/bot2-core.pc")
endif()

